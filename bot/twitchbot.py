from twitchio.ext import commands
import os
from discord.ext.commands import Bot
import logging

log = logging.getLogger(__name__)
token = os.environ.get('TWITCHBOT_TOKEN')
channels = ['zcleagues',]
bot_name = 'zclbotty'



class TwitchBot(commands.Bot):

    def __init__(self, discord_bot: Bot):
        self.bot = discord_bot
        log.debug(f"Discord Bot Passed: {discord_bot}")
        super().__init__(irc_token=token, nick=bot_name, prefix='!',
                         initial_channels=channels)
    # Events don't need decorators when subclassed
    async def event_ready(self):
        print(f'Ready | {self.nick}')

    async def event_message(self, message):
        log.info("Dispatching twitchbot message event")

        self.bot.dispatch("twitch_message", message)
        await self.handle_commands(message)



