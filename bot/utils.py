import datetime
import logging
from datetime import datetime as dt
from typing import Tuple

import discord
from dateutil import tz, parser
import random

from bot import constants
from typing import Optional
import pytz
import datetime
import typing
import re
import unicodedata

log = logging.getLogger(__name__)
EMPTYLINE = '\u200b'

class BrandedEmbed(discord.Embed):

    def __init__(self, **kwargs):
        colour = kwargs.get('colour')
        dashboard = kwargs.get('dashboard', False)
        if colour is None:
            colour = random.choice([discord.Colour.blue(),
                                    discord.Colour.red()])
            kwargs['colour'] = colour
        super(BrandedEmbed, self).__init__(**kwargs)
        self.set_author(name=constants.BRANDING_NAME,
                        url=constants.BRANDING_URL,
                        icon_url=constants.BRANDING_ICON)
        if not dashboard:
            # Auto updating dashboards really don't redraw well.
            # Only set this to those embeds that are static
            self.set_thumbnail(url=constants.BRANDING_ICON)

def replace_regional(input_str):
    chars = "🇦🇧🇨🇩🇪🇫🇬🇭🇮🇯🇰🇱🇲🇳🇴🇵🇶🇷🇸🇹🇺🇻🇼🇽🇾🇿"
    subs = 'abcdefghijklmnopqrstuvwxyz'
    result = input_str
    for i, c in enumerate(chars):
        result = result.replace(c, subs[i])
    return result

def remove_special_ez(input):
    es = '𝕖ⲈǝЭÉƎέɛ€ӛєɛᶒ𐎅ƸΣe̷͗͋Ⓔⓔ⒠EeḔḕḖḗḘḙḚḛḜḝẸẹẺẻẾếẼẽỀềỂểỄễỆệĒēĔĕĖėĘęĚěÈèÉéÊêËëȄȅȨȩȆȇƎⱻɆɇƏǝℰⱸℯ℮ℇƐ𝚎'
    zs = 'ⱿƶᶎzζⓏⓩ⒵ZzẐẑẒẓẔẕŹźŻżŽžȤȥⱫⱬƵƶɀℨℤ𝕫𝚣'
    for c in es:
        input = input.replace(c, 'e')

    for c in zs:
        input = input.replace(c, 'z')
    return input

def remove_accents(input_str):

    nfkd_form = unicodedata.normalize('NFKD', input_str)
    result = u"".join([c for c in nfkd_form if not unicodedata.combining(c)])
    result = remove_special_ez(input_str)
    return result


async def get_or_create_role(guild: discord.Guild, name: str, mentionable: bool = False) -> typing.Optional[discord.Role]:
    try:
        role = discord.utils.get(guild.roles, name=name)
        if role is None:
            role = await guild.create_role(name=name, mentionable=mentionable,
                                reason="Initial Setup of Role")
        return role
    except discord.errors.Forbidden:
        log.error(f"{guild} - Invalid permissions (manage_roles)")

async def discord_delete(o: typing.Any):
    try:
        await o.delete()
    except AttributeError:
        pass


async def move_to_voice(member: discord.Member, channel: discord.VoiceChannel, reason=None):
    if not isinstance(member, discord.Member):
        log.error(f"Member object type passed as {member}")
        return
    if not isinstance(channel, discord.VoiceChannel):
        log.error(f"VoiceChannel object type passed as {channel}")
        return
    try:
        await member.move_to(channel, reason=reason)
        await member.edit(mute=False)
    except (discord.errors.HTTPException, discord.errors.Forbidden) as e:
        log.warning(f"Could not move user {member.display_name}: {e}")




def uniques(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

def remove_dict_from_list(d: dict, seq: list) -> list:
    seq[:] = [o for o in seq if o.get('id') != d.get('id')]
    return seq

async def reaction(predicate: bool, ctx, emojis: Tuple[str, str] = (
        constants.EMOJIS['SUCCESS'], constants.EMOJIS['ERROR'])):
    if ctx.message is None:
        return

    true_emoji, false_emoji = emojis
    reaction = true_emoji if predicate else false_emoji
    return await ctx.message.add_reaction(reaction)

def remove_multiple_from_list(remove_seq: list, target_seq: list) -> list:
    target_seq = [remove_dict_from_list(o, target_seq) for o in remove_seq][-1]
    return target_seq

def timedelta_to_string(td) -> str:
    hours = int(td.seconds / 3600)
    mins = int(td.seconds / 60 % 60)
    days = td.days
    return f"{days} Days {hours} Hours {mins} Minutes"

def has_any_role(member: discord.Member, roles: Tuple[str]):
    roles = (discord.utils.get(member.guild.roles, name=r) for r in roles)
    return any(r in member.roles for r in roles)

def timedelta_condensed(td: datetime.timedelta):
        seconds = int(td.seconds)
        days, seconds = divmod(seconds, 86400)
        hours, seconds = divmod(seconds, 3600)
        minutes, seconds = divmod(seconds, 60)
        if days > 0:
            return '%dd%dh%dm%ds' % (days, hours, minutes, seconds)
        elif hours > 0:
            return '%dh%dm%ds' % (hours, minutes, seconds)
        elif minutes > 0:
            return '%dm%ds' % (minutes, seconds)
        else:
            return '%ds' % (seconds,)

def to_csv(key, seq, empty='Nothing to show.', sort=True) -> str:
    container = [o.get(key, "") for o in seq]
    container = sorted(container) if sort else container
    return ', '.join(container) or empty

def league_hosts(league: dict) -> list:
    owner = league.get('owner')
    hosts = league.get('hosts', [])
    if owner:
        hosts.append(owner)
    return hosts

def remove_keys_as_none(d: dict) -> dict:
    iter_dict = d.copy()
    for k in iter_dict.keys():
        if d[k] is None:
            del d[k]
        elif isinstance(d[k], dict):
            remove_keys_as_none(d[k])
    return d

def get_mod_log_channel(guild: discord.Guild) -> Optional[discord.TextChannel]:
    """
    Helper function to get the channel defined in constants for logging purposes.
    :param guild: The server guild for the channel
    :return:
    """
    ch = discord.utils.get(guild.channels, name=constants.MODERATION_LOG_CHANNEL)
    if ch:
        return ch

def get_next_day(weekday: int, hour=0, mins=0) -> datetime:
    onDay = lambda date, day: date + datetime.timedelta(days=(day - date.weekday() + 7) % 7)

    today = datetime.datetime.today()
    timezone = pytz.timezone('US/Central')
    target_date = onDay(today, weekday if today != weekday else weekday+1)
    target_time = datetime.time(hour=hour, minute=mins)
    result = datetime.datetime.combine(target_date, target_time)
    result = timezone.localize(result)
    return result

def get_team(teams: dict, site_members: list):
    member_ids = [p['id'] for p in site_members]
    log.debug(f"Got member ids: {member_ids}")
    for team in teams:
        log.debug(f"{member_ids} = {[p['id'] for p in team['players']]}")
        if len(team['players']) == len(member_ids):
            if all([p['id'] in member_ids for p in team['players']]):
                log.debug(f"FOUND TEAM: {team}")
                return team


async def send_to_mod_channels(guild, **kwargs):
    for ch in constants.MODERATION_CHANNELS:
        mod_channel = discord.utils.get(guild.channels, name=ch)
        if mod_channel:
            await mod_channel.send(**kwargs)
        else:
            log.error(f"Undefined channel in MODERATION_CHANNELS: {ch}")

async def send_to_log_channel(guild, **kwargs):
    log_ch = discord.utils.get(guild.channels, name=constants.MODERATION_LOG_CHANNEL)
    if log_ch:
        await log_ch.send(**kwargs)
    else:
        log.error(f"Undefined channel in MODERATION_LOG_CHANNEL: {ch}")

def is_admin(user: discord.Member):
    roles = discord.utils.get(user.roles, name='Admin')
    return roles is not None

async def delete_messages(msgs: typing.List[discord.Message]):
    for m in msgs:
        await delete_message(m)

async def delete_message(msg: Optional[discord.Message]):
    try:
        if msg is None:
            log.debug("delete_message got empty message object")
            return
        await msg.delete()
    except discord.errors.NotFound:
        log.error(f"Tried to delete non-existing Message {msg}")

def twitch_to_time_delta(twitch_time: str) -> Optional[datetime.timedelta]:
    """
           Format is
           1w2d1h18m2s

           :param last_active:
           :return:
           """
    pattern = '^(?:(?P<weeks>\d+)[w.])?(?:(?P<days>\d+)[d.])?(?:(?P<hours>\d+)[h.])?(?:(?P<minutes>\d+)[m.])?(?:(?P<seconds>\d+)[s.])?$'
    pattern = re.compile(pattern)
    matches = re.search(pattern, twitch_time)
    if matches is None:
        log.error(f"Invalid TimeDelta {twitch_time}")
        return
    args = {k: int(v) for k, v in matches.groupdict().items() if v and v.isdigit()}
    return datetime.timedelta(**args)

def to_datetime(date_string: str) -> datetime:
    """Parses into a python date time object.
    :param date_string  The string that associates a date/time.

    This will try to parse many different string representations of datetime
    into a python object.
    """
    return parser.parse(date_string)

def normalize_date(date_string: str,
                   to_timezone: str = constants.TIMEZONE) -> str:
    """Normalizes a datetime object into a human readable string.
    :param date_string  The string that associates a date/time.
    :param to_timezone  A valid Timezone to convert to. ex: `US/Central`
    :return str         A human readable string representation of a datetime
    """
    return to_datetime(date_string).astimezone(
        tz.gettz(to_timezone)).strftime('%D - %T')

def utc_now():
    return dt.now(datetime.timezone.utc)

"""
async def get_member_accounts(discord_members: discord.Member,
                                    player_collection = None):
    # helper function to help translate multiple discord users to a web
    # persisted user. It pull the whole User object and returns the records.
    if not player_collection:
        player_collection = await Player.all_records()
    discord_ids = [dm.id for dm in discord_members]

    result = [Player(p) for p in player_collection
              if p['discord_id'] in discord_ids]
    return result

async def to_site_ids(discord_members: discord.Member):
    return [p.id for p in await get_member_accounts(discord_members)]


async def to_discord_members(ctx: Context, players: List[Player]) -> List[discord.Member]:
    return [get(ctx.message.guild.members, id=p.discord_id)
            for p in players
            if get(ctx.message.guild.members, id=p.discord_id) is not None]

"""
