import logging
import os
import socket
from discord.ext.commands import Bot, command, Context, when_mentioned
from aiohttp import AsyncResolver, ClientSession, TCPConnector
from bot import constants
from bot.decorators import with_role
import discord
from .twitchbot import TwitchBot
import asyncio


TOKEN = os.environ['BOT_TOKEN']
auth_token = os.environ.get('API_AUTH_TOKEN', None)
auth_header = {'Authorization': f'Token {auth_token}'} if auth_token else None
log = logging.getLogger(__name__)
bot = Bot(
    command_prefix=',',

)
twitch_bot = TwitchBot(bot)
bot.http_session = ClientSession(
    headers=auth_header,
    connector=TCPConnector(
        resolver=AsyncResolver(),
        family=socket.AF_INET,
    )
)
extensions = [
    'bot.cogs.googletts',
    'bot.cogs.core',
    'bot.cogs.event',
    'bot.cogs.match',
    'bot.cogs.moderation',
    'bot.cogs.player',
    'bot.cogs.translate',
    'bot.cogs.twitchtv',
    'bot.cogs.notifications',
    'bot.cogs.zcl_client',
    'bot.cogs.lobby',
    'bot.cogs.queues',
    'bot.cogs.messenger',
]
constants.BOT = bot
constants.TWITCH_BOT = twitch_bot
def load_cogs(cogs: list = extensions):
    log.info(f"Loading COGS: {cogs}")
    for cog in cogs:
        try:
            #bot.unload_extension(cog)
            bot.load_extension(cog)
        except discord.ext.commands.ExtensionNotLoaded:
            pass

        except Exception as error:
            log.error(f'Could not load COG: {cog}. {error}')
            raise


@with_role(constants.ROLES['ADMIN'])
@bot.command(name='reset')
async def load_cogs_cmd(ctx, cogs: list = extensions):
    """ MODERATION: Reload the COG extensions of this Discord Bot.

    This will reset all memory state of the bot as well. Any data that was not
    persisted to the web api will be lost.
    """
    return load_cogs(cogs)



load_cogs()


log.info(f"Starting ZC4Bot with Debug option as {os.environ.get('DEBUG_MODE', False)} d.py Version {discord.__version__}")
bot.loop.set_debug(os.environ.get('DEBUG_MODE', False))

#bot.run(TOKEN)

async def discord_runner():
    try:
        await bot.start(TOKEN)
    finally:
        await bot.close()

async def twitchbot_runner():
    try:
        await twitch_bot.start()
    finally:
        await twitch_bot._ws.teardown()

def discord_callback(future: asyncio.Future):
    raise future.exception()


loop = asyncio.get_event_loop()
try:
    twitch = asyncio.ensure_future(twitchbot_runner(), loop=loop)
    discord = asyncio.ensure_future(discord_runner(), loop=loop).add_done_callback(discord_callback)
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    loop.close()



#bot.http_session.close()
