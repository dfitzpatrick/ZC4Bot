"""Discord BOT usable constants.
"""
import discord
from discord.ext.commands import Bot
from .twitchbot import TwitchBot
import typing

API_ROOT = 'https://localhost:8000/api/'
TIMEZONE = 'US/Central'
BOT_ROLES = ('Hosts',)
REGISTRATION_ROLE = 'Registered'
BOT: Bot = None
TWITCH_BOT: TwitchBot = None
OPEN_INVITATIONS = True
BRANDING_NAME = 'ZC LEAGUES'
BRANDING_URL = "http://www.zcleagues.com"
BRANDING_ICON = "https://i.ibb.co/h7M9RY4/logo-condensed-color.png"
PAYLOAD = typing.Dict[str, typing.Any]

URLS = {
    "site": "http://www.zcleagues.com",
    "youtube": "https://www.youtube.com/channel/UCKEytOemGNqOjmbRCqzydXQ",
    "twitch": "https://www.twitch.tv/zcleagues",
}
CHANNELS = {
    'saturday-signups': 493585774244134913,
    'general': 501213954476081172,
    'bot-commands': 508793605004197908,
}
ALLOW_BOT_COMMANDS_CHANNELS = [
    'check-in',
    'bot-and-website',
    'new-players',
]
EXCLUDE_COGS_ALLOW_BOT_COMMANDS = [
    'Translate'
]
TEMP_LEAGUES = [
    'Temp Pro League',
    'Temp Intermediate League'
]
LEAGUES = ['Professional League',
           'Intermediate League'
]

MODERATION_LOG_CHANNEL = 'log'
MODERATION_CHANNELS = [
    'contributor-lounge',
]
ROLES = {
    'REGISTERED': ('League',),
    'LEAGUES': ('Hosts', 'League Host',),
    'ADMIN': ('Hosts', 'Admin', 'Contributors', 'Moderators'),
    'MODERATORS': ('Admin', 'Contributors', 'Helpers'),
    'SOFA': ('sofamanager', 'Admin', 'Contributors'),

}
EMOJIS = {
    'SUCCESS': u"\u2705",
    'ERROR': u"\u274C",
    'CHECKIN': u"\u25B6",
    'TRUE': u"\uF535",
    'FALSE': u"\uF534",
    'THUMBS_UP': "\U0001f44d",
    'THUMBS_DOWN': "\U0001f44e",

}
PAGINATION_MAX_LINES = 10
LEAGUE_NON_MEMBER = """You are not a member of '{league}'.
To request membership, please contact one of the hosts: {hosts}"""
