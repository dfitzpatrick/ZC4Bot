import asyncio
import logging
import discord
from discord.ext import commands
import typing

from bot.mixins.config import ConfigMixin

log = logging.getLogger(__name__)

class Messenger(ConfigMixin, commands.Cog):
    
    def __init__(self, bot: commands.Bot):
        super(Messenger, self).__init__()
        self.bot = bot
        self.announcement_key = 'announcement_channels'
        self.logging_key = 'logging_channels'


    def get_config_value(self, guild: discord.Guild, key):
        guild_id = str(guild.id)
        try:
            result = self.config_settings[guild_id][key]
            return result
        except KeyError:
            return

    def embed_list(self, seq: typing.List[typing.Any], attr: str, title="Listing") -> discord.Embed:
        """
        Simple function to make a list of all entries of a List
        Parameters
        ----------
        seq: The sequence to evaluate
        attr: The attribute to apply on the object for the string result
        title: The discord embed title

        Returns
        -------
        discord.Embed
        """
        lst = '\n'.join(getattr(obj, attr) for obj in seq)
        embed = discord.Embed(title=title, description=lst)
        return embed

    async def configure_channels(self, ctx: commands.Context, channel: discord.TextChannel, key: str, title: str):
        guild_id = str(ctx.guild.id)
        cfg_channels = self.get_config_value(ctx.guild, self.announcement_key) or []
        if channel is None:
            channel_objs = [discord.utils.get(ctx.guild.text_channels, id=cid) for cid in cfg_channels]
            channel_objs = [ch for ch in channel_objs if ch is not None]
            await ctx.send(embed=self.embed_list(channel_objs, 'mention', title), delete_after=10)
            self.config_settings[guild_id][key] = [ch.id for ch in channel_objs]
            self.save_settings()
            return

        if channel.id in cfg_channels:
            cfg_channels.remove(channel.id)
        else:
            cfg_channels.insert(0, channel.id)

        self.config_settings[guild_id][key] = cfg_channels
        self.save_settings()

    async def send_to_channels(self, guild: discord.Guild, key, **kwargs) -> typing.List[typing.Dict[str, typing.Union[discord.TextChannel, discord.Message]]]:
        guild_id = str(guild.id)
        msgs = []
        cfg_channels = self.get_config_value(guild, key) or []
        initial_length = len(cfg_channels)
        for channel_id in cfg_channels:
            channel = discord.utils.get(guild.text_channels, id=channel_id)
            if channel is None:
                cfg_channels.remove(channel_id)
                continue
            msg = await channel.send(**kwargs)
            msgs.append({'channel': channel, 'msg': msg})
        if len(cfg_channels) < initial_length:
            self.config_settings[guild_id][key] = cfg_channels
            self.save_settings()
        return msgs

    async def logging_messenger(self, guild: discord.Guild, **kwargs):
        return await self.send_to_channels(guild, self.logging_key, **kwargs)

    async def announcement_messenger(self, guild: discord.Guild, **kwargs):
        return await self.send_to_channels(guild, self.announcement_key, **kwargs)

    @commands.group(name='messenger', invoke_without_command=False)
    async def messenger(self, ctx: commands.Context):
        pass

    @commands.has_permissions(manage_channels=True)
    @messenger.command(name='announcement')
    async def msg_announcement(self, ctx: commands.Context, channel: discord.TextChannel = None):
        await self.configure_channels(ctx, channel, self.announcement_key, "Announcement Channels")
        log.debug('announcement done')

    @commands.has_permissions(manage_channels=True)
    @messenger.command(name='logging')
    async def msg_logging (self, ctx: commands.Context, channel: discord.TextChannel = None):
        await self.configure_channels(ctx, channel, self.logging_key, "Logging Channels")
        log.debug('logging done')




def setup(bot: commands.Bot):
    bot.add_cog(Messenger(bot))
    log.info("Cog loaded: Messenger")
