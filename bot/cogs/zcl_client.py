import asyncio
import json
import logging
import os
from datetime import datetime, timezone, timedelta

import dateutil.parser
import discord
from atomicwrites import atomic_write
from discord.ext import commands, tasks


from bot import utils

log = logging.getLogger(__name__)

FILE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..', 'static'))
PATH = os.path.normpath(f'{FILE_DIR}/clients.json')

class ZCLClient(commands.Cog):
    """
    Actions related to interacting with the ZCL Electron Client
    """
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.loop = asyncio.get_event_loop()
        self.heartbeats = {}
        self.role_name = 'Client Connected'
        self.update_cli_role.start()

    def cog_unload(self):
        self.update_cli_role.cancel()

    def persist_config(self):
        with atomic_write(PATH, overwrite=True) as f:
            json.dump(self.heartbeats, f)

    def heartbeat_elapsed(self, heartbeat: str) -> timedelta:
        now = datetime.now(timezone.utc)
        heartbeat = dateutil.parser.parse(heartbeat)
        delta = now - heartbeat
        return delta

    async def update_guild_cli_role(self, guild: discord.Guild):
        role = await utils.get_or_create_role(guild, self.role_name)
        if role is None:
            return
        for m in role.members:
            m: discord.Member
            try:
                heartbeat = self.heartbeats[str(guild.id)][str(m.id)]
                delta = self.heartbeat_elapsed(heartbeat)
                if delta > timedelta(minutes=10):
                    await m.remove_roles(role, reason='Client Heartbeat Expired')
                    del self.heartbeats[str(guild.id)][str(m.id)]
                    self.persist_config()
            except KeyError:
                await m.remove_roles(role, reason='No Client Heartbeat')
            except (discord.errors.Forbidden, discord.errors.HTTPException) as e:
                log.error(e)
                return

    @tasks.loop(minutes=5)
    async def update_cli_role(self):
        for guild in self.bot.guilds:
            guild: discord.Guild
            await self.update_guild_cli_role(guild)


    @commands.command(name='client')
    async def client_promo(self, ctx: commands.Context):
        role = await utils.get_or_create_role(ctx.guild, self.role_name)
        num_users = len(role.members)

        description = f"""**The new client helps us get stats from all your replays**
        *{num_users} individuals are using the client right now.*\n
        [**Download the Latest Release**](https://github.com/dfitzpatrick/zcl-client/releases)
        
        __Features:__
        - Uploads your replays automatically. Including your old ones.
        - Updates your profile statistics.
        - Keeps your leaderboard and all its game modes up to date.
        - Links your accounts easily for our bot to manage.
        - Monitors game events like starting new games and sending alerts\n
        
        __Disclaimer:__
        This client uploads replays in the background. If you are on a metered connection 
        do not use this.
        
        """
        embed = discord.Embed(title="How to get the new Client",
                              description=description,
                              colour=discord.Colour.green())
        await ctx.send(embed=embed, delete_after=5*60)




    @commands.Cog.listener()
    async def on_ready(self):
        try:
            with open(PATH, 'r') as f:
                self.heartbeats = json.load(f)
        except FileNotFoundError:
            with open(PATH, 'a'):
                os.utime(PATH, None)
            self.persist_config()


    @commands.Cog.listener()
    async def on_user_update(self, guild: int, payload):
        log.debug('in user updated')
        guild_str = str(guild)
        member_id = payload['id']
        heartbeat = payload['client_heartbeat']
        log.debug(f"Member: {member_id}")
        if heartbeat is None:
            return

        if self.heartbeats.get(guild_str) is None:
            self.heartbeats[guild_str] = {}
        self.heartbeats[guild_str][member_id] = heartbeat
        delta = self.heartbeat_elapsed(heartbeat)

        # Only update if the heartbeat was recently changed.
        if delta < timedelta(minutes=1):
            guild: discord.Guild = self.bot.get_guild(guild)
            if guild is None:
                return

            member:discord.Member = guild.get_member(int(member_id))
            if member is None:
                return
            role = await utils.get_or_create_role(guild, self.role_name)
            if role not in member.roles:
                await member.add_roles(role, reason='Received Client Heartbeat')
                self.persist_config()

def setup(bot: commands.Bot):
    bot.add_cog(ZCLClient(bot))
    log.info("Cog loaded: ZCL Client Hooks")