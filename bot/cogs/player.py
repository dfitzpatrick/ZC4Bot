import logging
from collections import defaultdict
import discord
from discord.ext import commands
from discord.ext.commands import Bot, Context, Cog
from discord.utils import get

from bot import constants
from bot import webapi
from bot.decorators import with_role
from bot import restapi
from typing import Optional, Dict, Any
from bot import utils
import re

log = logging.getLogger(__name__)


async def create_new_player(user: discord.Member):
    """Invites a new player into our web api database by creating an account
    using their discord information. This will actively let us track players
    in matches. Each player must be invited before they can participate in
    a match.
    """

    def make_valid_username():
        """Prevent invalid characters from entering the database.
        """
        result = f"{user.name}{user.discriminator}"
        illegal_chars = '()!#$%^&*|;'
        for c in illegal_chars:
            result = result.replace(c, '')
        result = result.replace(' ', '_')
        return result

    log.info(f'Inviting new Player to Database: {user.display_name}')
    data = {
        'discord_id': user.id,
        'username': make_valid_username(),
        'display_name': user.display_name,
        'discord_discriminator': user.discriminator,
        'status': 'pending',
        'is_active': True,
    }
    site_player = await restapi.save('player', data)
    return site_player

async def get_or_create_site_member(player: discord.Member) -> Optional[Dict[str, Any]]:
    site_player = await restapi.get('discordplayer', player.id)
    if not site_player:
        # Create a new one
        log.debug(f"No Player Found for {player.display_name} in database. Attempting to create")
        site_player = await create_new_player(player)
        log.debug(f"New Record for: {player.display_name} in database. {site_player}")
    return site_player


class PlayerCOG(webapi.WebAPIMixin, Cog):
    """Manage and view statistics of ZC4 Players."""
    endpoint = 'player/'

    def __init__(self, bot: Bot, member: discord.Member = None):
        self.bot = bot
        self.discord_member = member
        self._user = None

    async def is_registered(self) -> bool:
        """Deprecated."""
        # Limit api calls. Lazy load the user and check if it exists
        return await self.user() is not None

    async def user(self):
        """Deprecated."""
        if self.discord_member and not self._user:
            id = self.discord_member.id
            site_member = self.get_record('discordplayer', id=id)
            if site_member:
                self._user = site_member
        return self._user

    @commands.group(name='player', invoke_without_commands=True)
    async def player_group(self, ctx: Context):
        """Commands for viewing ZC4 player stats and management."""
        pass



    @with_role(constants.ROLES['ADMIN'])
    @player_group.command('inviteme')
    async def player_inviteme(self, ctx: Context):

        data = {
            'discord_id': ctx.author.id,
            'display_name': ctx.author.name,
            'username': f"{ctx.author.name}{ctx.author.discriminator}",
            'discord_discriminator': ctx.author.discriminator,
            'status': 'pending',
            'is_active': True,
        }
        return self.api_save('player', data)



    async def player_invite_call(self, ctx: Context, user: discord.Member):
        """Invites a new player into our web api database by creating an account
        using their discord information. This will actively let us track players
        in matches. Each player must be invited before they can participate in
        a match.
        """
        def make_valid_username():
            """Prevent invalid characters from entering the database.
            """
            result = f"{user.name}{user.discriminator}"
            result = re.sub('[\W_]+', '', result, re.ASCII)
            return result

        log.info(f'Inviting new Player to Database: {user.name}')
        author = await self.get_record('discordplayer', id=ctx.author.id)
        data = {
            'discord_id': user.id,
            'username': make_valid_username(),
            'display_name': user.name,
            'discord_discriminator': user.discriminator,
            'status': 'pending',
            'is_active': True,
        }
        success = await self.api_save('player', data)
        return success

    @with_role(constants.ROLES['ADMIN'])
    @player_group.command('invite')
    async def player_invite(self, ctx: Context, *users: discord.Member):
        """Invites a new player into our web api database,

        This function acts by creating an account using their discord
        information. This will actively let us track players in matches. Each
        player must be invited before they can participate in a match.
        """
        for u in users:
            result = await self.player_invite_call(ctx, u)
            msg = 'Successful' if result else 'Unsuccessful'
            await ctx.send(f"Invitation to {u.name} {msg}")



    @player_invite.error
    async def player_invite_error(self, ctx: Context, error):
        """Error handling information for !player invite command"""
        log.error(f"ERROR ON INVITE: {error}")
        if isinstance(error, commands.BadArgument):
            await ctx.send("I cant find that user.")

    @player_group.command('stats')
    async def player_stats(self, ctx: Context, player: discord.Member):
        """Display the stats of a given player by their id or name.

        You can invoke this command using `!player stats [user]` the user arg
        can be either the discord user id, or their name (case-sensitive) or
        you can @mention their name directly.
        """
        site_player = await restapi.get('discordplayer', player.id)
        if not site_player:
            await ctx.send(f"Error: Could not find {player.display_name}'s web account.",
                           delete_after=5)
            return
        tournaments = await restapi.get('tournament')
        tournaments = [t for t in tournaments if t.get('status') == 'in progress']
        description = "Stats have now moved to the website!\n\n"
        for t in tournaments:
            pid = site_player.get('id')
            tid = t.get('id')
            name = t.get('name', 'No Name')
            url = f"http://www.zcleagues.com/stats/{pid}/?tournament={tid}"
            description += f"[{name}]({url})\n"
        embed = utils.BrandedEmbed(title=f"Stats for {player.display_name}",
                                   description=description)
        await ctx.send(embed=embed)



    """
    @player_group.command('stats')
    async def player_stats(self, ctx: Context, user: discord.Member):
        
        matches = await self.all_records('match', discordid=user.id)
        matches = [m for m in matches if m['status'] == 'completed']
        player_wins = []
        player_losses = []
        player_draws = []
        stats = defaultdict(dict)
        team_stats = defaultdict(dict)
        # Fix this.

        for match in matches:
            for team in match['teams']:
                for player in team['players']:
                    if user.id == player['discord_id']:
                        team_name = ' & '.join(p['display_name'] for p in team['players'])
                        if match.get('winning_team'):
                            if match['winning_team']['id'] == team['id']:
                                player_wins.append(match)
                                team_stats[team_name]['wins'] = team_stats[team_name].get('wins', 0) + 1
                            else:
                                player_losses.append(match)
                                team_stats[team_name]['losses'] = team_stats[team_name].get('losses', 0) + 1
                        else:
                            player_draws.append(match)
                            team_stats[team_name]['draws'] = team_stats[team_name].get('draws', 0) + 1

        stats = {
            'all': len(player_losses) + len(player_wins),
            'wins': len(player_wins),
            'losses': len(player_losses),
            'draws': len(player_draws),
        }
        if stats['all'] == 0:
            await ctx.send('This player has no games.')
            return

        ratio = stats['wins'] / stats['all']
        embed = discord.Embed(description=f"Stats for {user.name}")
        embed.add_field(name='Lifetime Matches', value=f"{stats['all']}")
        embed.add_field(name='Lifetime Wins', value=f"{stats['wins']}")
        embed.add_field(name='Lifetime Losses', value=f"{stats['losses']}")
        embed.add_field(name='Lifetime Draws', value=f"{stats['draws']}")
        embed.add_field(name='W/L Ratio', value=f"{ratio:.2f}")
        embed.add_field(name='-------------------------------------',
                        value=' W/L/D Team Statistics:', inline=False)
        log.debug(f"Team Stats: {team_stats}")
        for team in team_stats:

            wins = team_stats[team].get('wins', 0)
            losses = team_stats[team].get('losses', 0)
            draws = team_stats[team].get('draws', 0)
            total = wins + losses
            try:
                team_ratio = wins / losses
            except ZeroDivisionError:
                team_ratio = wins

            embed.add_field(name=f"{team}",
                            value=f"{wins}/{losses}/{draws} - "
                                  f"{team_ratio:.2f} ratio",
                            inline=False)
        await ctx.send(embed=embed)
        """
def setup(bot: Bot):
    """Responsible for loading the EventCOG extension into discord.py"""
    bot.add_cog(PlayerCOG(bot))
    log.info("Cog loaded: PlayerCOG")
