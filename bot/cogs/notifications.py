import asyncio
import json
import logging
import typing
import os
import websockets
from discord.ext.commands import Bot, Cog

log = logging.getLogger(__name__)

class Notifications(Cog):
    def __init__(self, bot: Bot):
        """ZCL Notification Service"""
        self.bot: Bot = bot
        self.loop = asyncio.get_event_loop()
        self._socket: typing.Optional[websockets.WebSocketClientProtocol] = None
        self._task: asyncio.Task = self.loop.create_task(self._listen())
        self._task.add_done_callback(self._listener_finished)

    @Cog.listener()
    async def on_ready(self):
        self.guild = self.bot.guilds[0]

    async def get_socket(self) -> websockets.WebSocketClientProtocol:
        base = os.environ.get('WS_HOST', 'ws://localhost:8000')
        #base = 'ws://localhost:8000'
        url = f'{base}/notifications/'
        if self._socket is None or not self._socket.open:
            log.debug(f"Opening connection to Notification channel {url}")
            self._socket = await websockets.connect(url)
        return self._socket

    @Cog.listener()
    async def on_match_event(self, guild: int, payload: typing.Optional[typing.Dict[str, typing.Any]]):
        """
        These are common match events. They are easily deconstructed and common so we fire
        them as different events here for the rest of the bot to consume
        Parameters
        ----------
        guild
        payload

        Returns
        -------

        """
        action = payload['type']
        log.debug('in on_match_event')
        log.debug(f'dispatching {action}')
        self.bot.dispatch(action, guild, payload)

    def _listener_finished(self, task: asyncio.Task):
        """
        Returns
        -------
        None
        """
        try:
            if task.exception():
                log.error(task.print_stack())

                log.error("Websocket Consumer crashed to exception.")
                asyncio.ensure_future(asyncio.sleep(30))

                self._task = self.loop.create_task(self._listen())
                self._task.add_done_callback(self._listener_finished)


        except asyncio.futures.CancelledError:
            pass

    async def _listen(self):
        """
        Listens to the websocket stream for ZCL Notifications
        :return:
        """
        while True:
            socket = await self.get_socket()
            data = await socket.recv()
            log.debug(f'Data received: {data}')
            data = json.loads(data)
            for k in data:
                print(k, type(data[k]))


            # Get the payload if any
            payload = data.get('payload')
            guild = 476518371320397834
            #guild = discord.utils.get(self.bot.guilds, id=payload['guild'])
            log.debug(f"sending dispatch on_{data['action']}")
            self.bot.dispatch(data['action'], guild, payload)


    def __del__(self):
        if self._socket is not None and self._socket.open:
            self._socket.close()

def setup(bot: Bot):
    bot.add_cog(Notifications(bot))
    log.info("Cog loaded: ZCL Notfications")