import asyncio
import json
import logging
import os
from collections import defaultdict
from datetime import datetime, timezone

import discord
from atomicwrites import atomic_write
from discord.ext import commands
from discord.ext.commands import Bot, Context, Cog
from discord.utils import get

from bot import utils

log = logging.getLogger(__name__)
UPDATE_DELAY = 120
STREAM_ROLE_NAME = 'Streamer'

FILE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..', 'static'))
PATH = os.path.normpath(f'{FILE_DIR}/twitchtv.json')

class TwitchTV(Cog):

    def __init__(self, bot: Bot):
        self.bot = bot
        self.config = defaultdict(dict)
        self.data = []
        self.streams = {}


    def cog_unload(self):
        pass

    def on_ready(self):
        with open(PATH, 'r') as f:
            self.streams = json.load(f)

        for guild_id, data in self.streams.items():
            guild:discord.Guild = discord.utils.get(self.bot.guilds, id=guild_id)
            if guild is None:
                continue

            for member_id, broadcast in data.get('broadcasts', {}):
                channel_id = broadcast['channel_id']
                message_id = broadcast['message_id']
                channel: discord.TextChannel = discord.utils.get(guild.text_channels, id=channel_id)
                if channel is None:
                    continue
                message = asyncio.ensure_future(channel.fetch_message(message_id))
                if message is not None:
                    asyncio.ensure_future(message.delete())
            self.streams[guild_id]['broadcasts'] = {}
        self.persist_config()


    def persist_config(self):
        with atomic_write(PATH, overwrite=True) as f:
            json.dump(self.config, f)

    def add_streamer(self, guild_id: int, member_id: int, channel_id: int, message_id: int):
        initial_guild = {
            'broadcasts': {},
        }
        guild_id = str(guild_id)
        member_id = str(member_id)
        if self.streams.get(guild_id) is None:
            self.streams[guild_id] = initial_guild
        self.streams[guild_id]['broadcasts'][member_id] = {
            'channel_id': channel_id,
            'message_id': message_id,
        }
        self.persist_config()

    @Cog.listener()
    async def on_stream_stop(self, guild_id: int, payload):
        log.debug(payload)
        user_id_str = payload['user_id']
        guild_id_str = str(guild_id)
        try:
            message_id = self.streams[guild_id_str]['broadcasts'][user_id_str]['message_id']
            channel_id = self.streams[guild_id_str]['broadcasts'][user_id_str]['channel_id']
            guild: discord.Guild = discord.utils.get(self.bot.guilds, id=guild_id)
            channel:discord.TextChannel = discord.utils.get(guild.text_channels, id=channel_id)
            message = await channel.fetch_message(message_id)
            await message.delete()
            del self.streams[guild_id_str]['broadcasts'][user_id_str]
        except (KeyError, AttributeError, discord.errors.NotFound, discord.errors.HTTPException, discord.errors.Forbidden):
            pass

    @Cog.listener()
    async def on_stream_start(self, guild_id: int,  payload):
        log.debug(payload)
        if payload.get('user_id') is None:
            return
        user_id = int(payload['user_id'])
        guild: discord.Guild = discord.utils.get(self.bot.guilds, id=guild_id)
        channel: discord.TextChannel = discord.utils.get(guild.text_channels, name='general')
        member: discord.Member = discord.utils.get(guild.members, id=user_id)
        data = payload['data'][0]
        thumbnail = data['thumbnail_url'].format(width=256, height=256)
        title = data['title']
        username = data['user_name']
        stream_url = f'https://twitch.tv/{username}'
        description = f"""{member.display_name} is now streaming!\n
            [{title}]({stream_url})
        """

        embed = discord.Embed(
            title=f"{member.display_name}'s Stream is now Live",
            colour=discord.Colour.teal(),
            description=description
        )
        embed.set_thumbnail(url=thumbnail)
        embed.timestamp = datetime.now(timezone.utc)
        msg = await channel.send(embed=embed)
        self.add_streamer(guild_id, user_id, channel.id, msg.id)


    @Cog.listener()
    async def on_new_match_stream(self, guild: discord.Guild, payload):
        log.debug('received new_match stream')
        log.debug(type(payload))
        log.debug(payload)

        embed = utils.BrandedEmbed(
            title="Stream Alert"
        )
        players = [p['name'] for p in payload['players']]
        streamer_value = ""
        thumb = payload['streamers'][0]['stream']['extra_data']['data'][0]['thumbnail_url'].format(width=64,height=64)
        for p in payload['streamers']:
            stream_name = p['stream']['extra_data']['data'][0]['title']
            viewers = p['stream']['extra_data']['data'][0]['viewer_count']
            text = f"[{p['profile']['name']}/{stream_name}](https://twitch.tv/{p['stream']['username']}) - {viewers} watching\n"
            streamer_value += text
        embed.add_field(name='Players', value=', '.join(players) or 'No Players', inline=False)
        embed.add_field(name='Streamers', value=streamer_value, inline=False)
        embed.set_thumbnail(url=thumb)
        embed.timestamp = datetime.now(timezone.utc)
        embed.set_footer(text=f"{len(payload['streamers'])} streaming...")
        guild = self.bot.guilds[0]
        ch = discord.utils.get(guild.channels, name='general')
        await ch.send(embed=embed)


    @commands.cooldown(1, 120, commands.BucketType.guild)
    @commands.group(name='stream', invoke_without_command=True)
    async def twitch_stream(self, ctx: Context):
        """
        Displays an embed to the channel of tracked streamers and their streaming status.
        """
        embed = utils.BrandedEmbed(title='ZC League Streamers')
        # f-string can't put backslash
        nl = '\n'
        online_markdown = [f"[{streamer['user_name']} - {streamer['viewer_count']} Viewers](https://www.twitch.tv/{streamer['user_name']})"
                           for streamer in self.data]
        offline_markdown = [f"[{streamer}](https://www.twitch.tv/{streamer})" for streamer in self.offline_streamers()]
        embed.add_field(name="Active Streams", value=f"{nl.join(online_markdown) or 'No Active Streams'}", inline=False)
        embed.add_field(name="Offline Streams", value=f"{nl.join(offline_markdown)}" or 'No Offline Streams', inline=False)
        await ctx.send(embed=embed, delete_after=120)




    @Cog.listener()
    async def on_member_update(self, before_user: discord.Member,
                               after_user: discord.Member):
        """
        Tracks when a member updates their status and looks to see if they
        started "Streaming". Add a specific role to them to call them to the
        top of the discord list.
        :param before_user: The Member state before the update happened
        :param after_user: The Member state after the update happened
        :return: None
        """
        stream_role = get(after_user.guild.roles or [], name=STREAM_ROLE_NAME)
        if not stream_role:
            return

        if any(isinstance(a, discord.Streaming) for a in after_user.activities):
            log.debug(f"{after_user.display_name} Started Streaming")
            if stream_role not in after_user.roles:
                await after_user.add_roles(stream_role,
                                           reason="Started Streaming Activity",
                                           atomic=True)
        else:
            if stream_role in after_user.roles:
                await after_user.remove_roles(stream_role,
                                              reason="Ended Streaming Activity",
                                              atomic=True)

    @Cog.listener()
    async def on_command_completion(self, ctx: Context):
        if ctx.cog != self:
            return
        await utils.reaction(True, ctx)
        await asyncio.sleep(5)
        await utils.delete_message(ctx.message)

    async def cog_command_error(self, ctx: Context, error: discord.ext.commands.CommandError):
        if ctx.cog != self:
            return
        log.error(error)
        error.with_traceback(error.__traceback__)
        await utils.reaction(False, ctx)
        await asyncio.sleep(5)
        await utils.delete_message(ctx.message)
        raise error


def setup(bot: Bot):
    bot.add_cog(TwitchTV(bot))
    log.info("Cog loaded: TwitchTV")




