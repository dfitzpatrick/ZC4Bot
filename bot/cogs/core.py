import logging
from typing import Dict, Any

import discord
from discord.ext.commands import Context, Bot, command, Cog
from discord.utils import get

from bot import constants
from bot import restapi
from bot import utils
from bot.cogs.moderation import Moderation
from bot.cogs.zcl_client import ZCLClient
from bot.cogs.translate import Translate
from bot.cogs.twitchtv import TwitchTV
from bot.cogs.event import EventCOG
from bot.constants import BOT as bot
from bot.decorators import with_role
from discord.ext import commands
from bot import checks
import os
import asyncio
import datetime
import pickle
import collections
from bot.cogs.googletts import GoogleTTS
from bot.pagination import LinePaginator
from bot.constants import TWITCH_BOT as twitch_bot

log = logging.getLogger(__name__)

FILE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..', 'static'))
USER_ACTIVITY_TEMP = os.path.normpath(f'{FILE_DIR}/user_activity.temp')
USER_ACTIVITY_PATH = os.path.normpath(f'{FILE_DIR}/user_activity.pickle')
GREETING_TIME = 60*30

GRANTED_ACCESS = """
Congratulations {player}! You've been granted access to this server.

**__Next Steps:__**

-> __Bookmark:__
   [Website/Stats]({site})  
   [Youtube]({youtube})
   [Twitch Stream]({twitch})


-> __Get Coaching:__
   Head over to {coaching} to find a mentor and work on your skills.
   
   
-> __Join A League/Tournament:__
   We hold various tournaments from time to time.
   
   The CE Tournament is open to all players on Saturdays 3pm CT.
   
   League Nights require membership. See the rules document for entry information.
   
   *To call up a list of events type `,e info` in the {bot_commands}*
"""




async def new_player_message(messageable):
    description = """Welcome to the new Zone Control Server!

    This server is dedicated to helping organize the Zone Control community and
    help increase the popularity of the game we love to play. This discord is
    broken down into a series of League's that players can be invited into and
    participate. What day each league plays is up the individual hosts to arrange.

    Here is what you need to do:
    1. Read all the rules in #intro-and-rules
    2. We require voice verification. Type @Moderator verify in #start-here when you are ready.
    3. You can opt into our league system once you are verified by typing ,optin
    
    4. After opting in you can ping @Level 0 if you are looking for a game.
    
    """

    embed = utils.BrandedEmbed(title='Welcome to the server!',
                  description=description)

    await messageable.send(embed=embed)




class Core(Cog):

    def __init__(self, bot: Bot):
        self.bot = bot
        self.user_activity = {}
        self.user_activity_task = None
        self.voice_announcement_flag = {}
        self.googletts: GoogleTTS = self.bot.get_cog('GoogleTTS')
        log.debug(f"TTS: {self.googletts}")
        self.messenger = None

    def commit_activity_to_disk(self):
        with open(USER_ACTIVITY_TEMP, 'wb') as f:
            pickle.dump(self.user_activity, f)
        os.replace(USER_ACTIVITY_TEMP, USER_ACTIVITY_PATH)


    @Cog.listener()
    async def on_ready(self):

        try:
            self.messenger = self.bot.get_cog('Messenger')
            with open(USER_ACTIVITY_PATH, 'rb') as f:
                # self.schedules = json.load(f, object_hook=datetime_parser)
                # log.debug(self.schedules)
                self.user_activity = pickle.load(f)
        except FileNotFoundError:
            pass
        self.user_activity_task = self.bot.loop.create_task(self.user_activity_task_loop())
        self.user_activity_task.add_done_callback(self.user_activity_task_finished)

    async def user_activity_task_loop(self):
        while True:
            self.commit_activity_to_disk()
            await asyncio.sleep(15)


    def user_activity_task_finished(self, task: asyncio.Task):
        if task.exception():
            log.error("Exception found in User Activity Task. Task has terminated")
            task.print_stack()

    async def send_granted_embed(self, ctx: Context, player: discord.Member):
        """
        Sends a pre-formated embed for the player
        :param player:
        :return:
        """
        log.debug('in function')
        name = player.display_name
        needs_orientation = get(ctx.guild.roles, name='Orientation') in player.roles
        #if needs_orientation:
        #    orientation = "You must complete your orientation. Message back with 'Hi' to start."
        #else:
        #    orientation = "Orientation has been completed! Thanks!"

        coaching = get(ctx.guild.channels, name='coaching')
        bot_commands = get(ctx.guild.channels, name='bot-and-website')
        coaching = coaching.mention if isinstance(coaching, discord.TextChannel) else '#coaching'
        bot_commands = bot_commands.mention if isinstance(bot_commands, discord.TextChannel) else '#bot-and-website'

        embed = utils.BrandedEmbed(title=f"Congratulations {name}, and Next Steps",
                                   description=GRANTED_ACCESS.format(
                                       player=player.display_name,
                                       site=constants.URLS.get('site'),
                                       youtube=constants.URLS.get('youtube'),
                                       twitch=constants.URLS.get('twitch'),
                                       coaching=coaching,
                                       bot_commands=bot_commands,
                                   )
        )
        try:
            await player.send(embed=embed)
        except discord.errors.Forbidden:
            await ctx.send(f"I'm posting this here since {name} doesn't accept DM's. This will delete after 2 minutes",
                           delete_after=120)
            await ctx.send(embed=embed, delete_after=120)

    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    @command(name='activity')
    async def user_activities(self, ctx: Context):
        summary = utils.BrandedEmbed(title="User Summary")
        member_count = len(ctx.guild.members)
        member_ids = [m.id for m in ctx.guild.members]
        registered_role = discord.utils.get(ctx.guild.roles, name='Registered')
        embed = utils.BrandedEmbed(title="Activities")
        times = {
                    discord.utils.get(ctx.guild.members, id=int(k))
                    :
                    (datetime.datetime.utcnow() - v).days
                    for k,v in self.user_activity.items()
                    if discord.utils.get(ctx.guild.members, id=int(k)) is not None
                 }

        times = collections.OrderedDict(sorted(times.items(), key=lambda t: t[1], reverse=True))
        active_7 = len([v for k, v in times.items() if v <= 7 and k.id in member_ids])
        active_14 = len([v for k,v in times.items() if v <= 14 and k.id in member_ids]) - active_7
        active_30 = len([v for k,v in times.items() if v <= 30 and k.id in member_ids]) - active_7 - active_14
        untracked = list(m.display_name for m in ctx.guild.members if str(m.id) not in self.user_activity.keys())
        no_activity = len(untracked)

        over_30 = member_count - active_7 - active_14 - active_30 - no_activity
        pending_registration = [m.display_name for m in ctx.guild.members if registered_role not in m.roles] if registered_role is not None else []


        summary.add_field(name="Total Members", value=str(member_count))
        summary.add_field(name="Active Last 7 Days", value=f"{active_7} ({round((active_7 / member_count)*100, 1)}%)")
        summary.add_field(name="Active Last 14 Days", value=f"{active_14} ({round((active_14 / member_count)*100, 1)}%)")
        summary.add_field(name="Active Last 30 Days", value=f"{active_30} ({round((active_30 / member_count)*100,1)}%)")
        summary.add_field(name="Inactive Over 30", value=f"{over_30} ({round((over_30/member_count)*100,1)}%)")
        summary.add_field(name="No Activity", value=f"{no_activity} ({round((no_activity / member_count) * 100,1)}%)")
        summary.add_field(name=f"Pending Registration", value=str(len(pending_registration)))
        await ctx.send(embed=summary)
        if pending_registration:
            await LinePaginator.paginate(
                lines=(m for m in pending_registration),
                ctx=ctx,
                embed=discord.Embed(title="Members pending Registration. Red X to continue"),
                empty=False,
                max_lines=20,
            )
        if untracked is not None:
            await LinePaginator.paginate(
                lines=(m for m in untracked),
                ctx=ctx,
                embed=discord.Embed(title="Members without Activity. Red X to continue"),
                empty=False,
                max_lines=20,
            )
        if len(times) < 1:
            await ctx.send("No activity to display.")
            return
        await LinePaginator.paginate(
            lines=(f"{k.display_name}: {v} Days" for k, v in times.items()),
            ctx=ctx,
            embed=embed,
            empty=False,
            max_lines=20,
        )

    @commands.has_any_role(*constants.ROLES['ADMIN'])
    @commands.command(name='inactive')
    async def inactive_players_past(self, ctx: Context, num_days: int):
        member_ids = [m.id for m in ctx.guild.members]
        role: discord.Role = discord.utils.get(ctx.guild.roles, name='Verified')
        times = {
            discord.utils.get(ctx.guild.members, id=int(k))
            :
                (datetime.datetime.utcnow() - v).days
            for k, v in self.user_activity.items()
            if discord.utils.get(ctx.guild.members, id=int(k)) is not None
        }
        inactive = [k for k, v in times.items() if v >= num_days and k.id in member_ids and role in k.roles]
        member_names = [m.display_name for m in inactive]
        member_names.sort()

        members = ""
        for m in member_names:
            if len(members + f", {m}") > 1900:
                embed = discord.Embed(title=f"Inactive Verified members {num_days} days", description=members)
                await ctx.send(embed=embed)
                members = f"{m}"
            elif len(members) == "":
                members = f"{m}"
            else:
                members = members + f", {m}"
        embed = discord.Embed(title="Inactive Verified members {num_days} days", description=members)
        await ctx.send(embed=embed)

    @command(name='announcements')
    @checks.role_exists('Announcements')
    @commands.cooldown(rate=1, per=120, type=commands.BucketType.member)
    async def add_announcements_role(self, ctx: Context):
        """
        Adds the Announcement role to the user if they do not have it,
        or removes it if they are currently assigned.
        :param ctx:
        :return:
        """
        role = get(ctx.guild.roles, name='Announcements')
        author = ctx.author
        if role in author.roles:
            await author.remove_roles(role)
            await author.send('You have been unsubscribed to Announcements')
        else:
            await author.add_roles(role)
            await author.send('You have been subscribed to Announcements')


    @add_announcements_role.error
    async def add_announcements_role_error(self, ctx: Context, error: Any):
        if isinstance(error, checks.RoleDoesNotExist):
            role = await utils.get_or_create_role(ctx.guild, name='Announcements', mentionable=True)
            if role is not None:
                await ctx.invoke(self.add_announcements_role)


    @command(name='grant')
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def grant_entry(self, ctx: Context, player: discord.Member):
        role = get(ctx.guild.roles, name='Registered')
        if role in player.roles:
            await ctx.author.send('This player is already granted entry')
            return
        await player.add_roles(role)
        await ctx.send('Entry granted', delete_after=5)
        await self.send_granted_embed(ctx, player)

    @grant_entry.error
    async def grant_entry_error(self, ctx: Context, error: Any):
        if isinstance(error, checks.RoleDoesNotExist):
            role = await utils.get_or_create_role(ctx.guild, name='Registered', mentionable=True)
            if role is not None:
                await ctx.invoke(self.grant_entry, **ctx.args)


    @with_role(constants.ROLES['ADMIN'])
    @command(name='onjoin')
    async def mimic_on_join(self, ctx: Context):
        """
        Simulates a member joining the discord. Legacy method to show what
        the message a new player receives is.
        :param ctx: Context
        :return: None
        """
        bot.dispatch('member_join', ctx.author)

    async def register_player(self, member: discord.Member) -> Dict[str, Any]:
        """
        Creates a new Player record in the postgres database.
        :param member: Discord Member
        :return: Dictionary of Player instance.
        """
        def make_valid_username():
            """Prevent invalid characters from entering the database.
            """
            result = f"{member.name}{member.discriminator}"
            illegal_chars = '()!#$%^&*|;'
            for c in illegal_chars:
                result = result.replace(c, '')
            result = result.replace(' ', '_')
            return result

        log.info(f'Inviting new Player to Database: {member.name}')
        data = {
            'discord_id': member.id,
            'username': make_valid_username(),
            'display_name': member.name,
            'discord_discriminator': member.discriminator,
            'status': 'pending',
            'is_active': True,
        }
        success = await restapi.save('player', data)
        return success


    @with_role(constants.ROLES['ADMIN'])
    @command(name='getavatar', aliases=('ga',))
    async def get_avatar(self, ctx:Context, player: discord.Member):
        """
        Returns the URL to the player's avatar. Mainly used for debugging.
        :param ctx: Context
        :param player: Discord Member
        :return:
        """
        await ctx.send(player.avatar_url)

    @with_role(constants.ROLES['ADMIN'])
    @command(name='playerupdate', aliases=('pu',))
    async def player_update(self, ctx: Context):
        """
        Updates player avatars in the database, and shows potential mismatches.
        This will output to contributor-lounge channel.
        :param ctx: Context
        :return: None
        """

        members = ctx.guild.members
        await ctx.trigger_typing()
        await utils.send_to_log_channel(ctx.guild, content="Processing Player Update and updating avatars...")
        for m in members:
            p = await restapi.get('discordplayer', m.id)
            if p:
                p['discord_avatar_url'] = str(m.avatar_url)
                if p['display_name'] != m.display_name:
                    await utils.send_to_log_channel(ctx.guild, content=f"Found name mismatch. Discord: {m.display_name} || Database: {p['display_name']}. Updating...")
                    p['display_name'] = m.display_name
                await restapi.save('player', p)
            else:
                await utils.send_to_log_channel(ctx.guild, content=f"No web account for {m.display_name}: id: {m.id}")


    @with_role(constants.ROLES['ADMIN'])
    @command(name='matcherrors', aliases=('me',))
    async def match_errors(self, ctx: Context):
        """
        Administrative function designed to find common pitfalls in how matches
        are categorized. These apply to ranked matches and outputs to the
        contributor-lounge.
        :param ctx: Context
        :return: None
        """
        admin_channel = get(ctx.guild.channels, name='contributor-lounge')
        matches = await restapi.get('match')
        lines = ()
        for m in matches:
            if m['ranked']:
                if m['tournament'] is None:
                    lines += (f"Match {m['id']} - No Tournament Selected",)
                elif m['league'] is None:
                    lines += (f"Match {m['id']} - No League Selected",)
                elif len(m['teams']) == 2 and m['tournament']['name'] == 'Season 1':
                    lines += (f"Match {m['id']} - Possible 1v1 or 4v4 attached to Season 1",)

        await admin_channel.send("Listing possible Match errors...")
        for l in lines:
            await admin_channel.send(l)

    @Cog.listener()
    async def on_voice_state_update(self, member: discord.Member, before, after):
        activity_key = str(member.id)
        self.user_activity[activity_key] = datetime.datetime.utcnow()

        if self.googletts is None or after.channel is None:
            return
        if before.channel is not None:
            return

    @Cog.listener()
    async def on_message(self, message:discord.Message):
        """
        Provides as an orientation function.
        :param message:
        :return:
        """
        def check(msg):
            return msg.author == message.author and msg.channel == message.channel

        async def agrees():
            r = await self.bot.wait_for('message', check=check)
            return r.content.lower().startswith('yes')

        activity_key = str(message.author.id)
        self.user_activity[activity_key] = datetime.datetime.utcnow()


        # Hard coded to single guild
        guild = self.bot.guilds[0]
        member = get(guild.members, id=message.author.id)
        if member is None:
            return

        orientation_role = await utils.get_or_create_role(guild, name='Orientation')
        if any([
            not isinstance(message.channel, discord.DMChannel),
            orientation_role not in member.roles,
            not message.content.lower().startswith('hi')
        ]):
            return
        try:
            await member.send('Placeholder 1')
            await member.send('Do you accept and agree to our rules? Please type yes or no')
            if not await agrees():
                await member.send('Let me know when you are ready to agree')
                return

            await member.send('Placeholder 2')
            await member.send('Are you interested in receiving coaching?')
            if await agrees():
                coach_role = await utils.get_or_create_role(guild, name='Coach', mentionable=True)
                coach_channel = get(guild.channels, name='coaching')
                description = f"A new player, {member.display_name}, going through orientation is interested in being coached."
                embed = utils.BrandedEmbed(title=f"{member.display_name} is interested in coaching", description=description)
                if coach_channel:
                    await coach_channel.send(f"{coach_role.mention}")
                    await coach_channel.send(embed=embed)
                    await member.send(f'Our coaching channel can be found at {coach_channel.mention}')
                await utils.send_to_mod_channels(guild, embed=embed)
                await member.send('Your request for coaching has been sent. Look for responses after orientation completes')


            await member.send('Placeholder 3')
            await member.send('Would you like to get notified of our announcements?')
            if await agrees():
                announcements_role = await utils.get_or_create_role(guild, name='Announcements', mentionable=True)
                await member.add_roles(announcements_role)
                await member.send('Subscribed!')

            await member.send('Placeholder 4')
            await member.send('Would you like to join the Up & Coming League')
            if await agrees():
                uac_role = await utils.get_or_create_role(guild, name='Up and Coming League', mentionable=True)
                await member.add_roles(uac_role)
                await member.send('Joined!')

            await member.send('Thanks! You are all set! Enjoy your stay!')
            await member.remove_roles(orientation_role)


        except discord.errors.Forbidden:
            pass



    @with_role(constants.ROLES['MODERATORS'])
    @commands.command(name='league-membership')
    async def league_role_membership(self, ctx: Context):
        roles = [get(ctx.guild.roles, name=r) for r in constants.TEMP_LEAGUES + constants.LEAGUES]
        roles = [r for r in roles if r is not None]
        embed = utils.BrandedEmbed(title='League Membership')
        for r in roles:
            players = '\n'.join(m.display_name for m in r.members) or 'No Players'
            embed.add_field(name=r.name, value=players)
        await ctx.send(embed=embed)

    @with_role(constants.ROLES['ADMIN'])
    @commands.command(name='temp-votes')
    async def vote_temp_memberships(self, ctx: Context):
        pro_temp = get(ctx.guild.roles, name='Temp Pro League')
        int_temp = get(ctx.guild.roles, name='Temp Intermediate League')

        pro_ch = get(ctx.guild.channels, name='pro-lounge')
        int_ch = get(ctx.guild.channels, name='intermediate-lounge')

        for m in pro_temp.members:
            m:discord.Member
            name = m.display_name
            msg:discord.Message = await pro_ch.send(f"{name} For Pro League?")
            await msg.add_reaction(constants.EMOJIS['THUMBS_UP'])
            await msg.add_reaction(constants.EMOJIS['THUMBS_DOWN'])

        for m in int_temp.members:
            name = m.display_name
            msg:discord.Message = await int_ch.send(f"{name} For Intermediate League?")
            await msg.add_reaction(constants.EMOJIS['THUMBS_UP'])
            await msg.add_reaction(constants.EMOJIS['THUMBS_DOWN'])

    @with_role(constants.ROLES['ADMIN'])
    @commands.command(name='clean-leagues')
    async def clean_leagues(self, ctx: Context):
        pro_role = get(ctx.guild.roles, name='Professional League')
        int_role = get(ctx.guild.roles, name='Intermediate League')

        for m in pro_role.members:
            m:discord.Member
            if m in int_role.members:
                await m.remove_roles(int_role)
                await utils.send_to_log_channel(ctx.guild, content=f"Removed {m.display_name} from {int_role.name}")

    @commands.command(name='optin')
    async def opt_into_leagues(self, ctx: Context):
        author: discord.Member = ctx.author
        verified = await utils.get_or_create_role(ctx.guild, "Verified")
        if verified is not None and verified not in author.roles:
            await ctx.send("You must be verified to use this command", delete_after=20)
            return

        role = await utils.get_or_create_role(ctx.guild, 'Level 0', mentionable=True)
        if role is None:
            await self.messenger.logging_messenger(ctx.guild, "Error: There is no Level 0 role configured. Find an Admin to help")
            await ctx.send("Error: There is no Level 0 role configured. Find an Admin to help", delete_after=20)
            return

        msg = ""
        if role in author.roles:
            await author.remove_roles(role)
            msg = f"{author.mention} is now opted out of the Tiered League system starting at Level 0"
        else:
            await author.add_roles(role)
            msg = f"{author.mention} is now opted into the Tiered League system starting at Level 0"
        await ctx.send(msg, delete_after=10)
        await self.messenger.logging_messenger(ctx.guild, content=msg)

    @with_role(constants.ROLES['ADMIN'])
    @commands.command(name='verified')
    async def opt_into_leagues(self, ctx: Context):
        role:discord.Role = discord.utils.get(ctx.guild.roles, name='Verified')
        if role is None:
            return
        members = ""
        member_names = [m.display_name for m in role.members]
        member_names.sort()
        for m in member_names:
            if len(members + f", {m}") > 1900:
                embed = discord.Embed(title="Verified members", description=members)
                await ctx.send(embed=embed)
                members = f"{m}"
            elif len(members) == "":
                members = f"{m}"
            else:
                members = members + f", {m}"
        embed = discord.Embed(title="Verified members", description=members)
        await ctx.send(embed=embed)

@bot.event
async def on_member_join(member: discord.Member):
    """
    Dispatches the welcome message to the player as a DM as well as a server
    side announcement in the #general chat channel.
    :param member: Discord Member
    :return:
    """

    core = Core(bot)
    p = await restapi.get('discordplayer', member.id)
    if not p and constants.OPEN_INVITATIONS:
        await core.register_player(member)
    await new_player_message(member)

    general_channel = get(member.guild.channels, name='general')
    embed = utils.BrandedEmbed(title=f"Welcome {member.name}!",
                               description=f"Please welcome our newest member to the "
                                           f"ZC Leagues discord: {member.name}!")
    if general_channel:
        await general_channel.send(embed=embed)
    await utils.send_to_log_channel(member.guild, embed=embed)




def setup(bot: Bot):
    bot.add_cog(Core(bot))
    log.info("Cog loaded: Core")
