import asyncio
import logging
from datetime import datetime as dt
from datetime import timedelta
from datetime import timezone
from typing import Dict, Optional, Any, List

import discord
from discord.ext import commands
from discord.ext.commands import Bot, Context, Cog
from discord.utils import get

from bot import constants
from bot import restapi
from bot import utils
from bot.cogs.player import get_or_create_site_member
from bot.decorators import with_role
import pytz

log = logging.getLogger(__name__)


class EventCOG(Cog):
    """
    This COG intention is for Event Management and community engagement around
    displaying events for users to reserve into. The main goals of this are to:
    1) Allow players to reserve via a dashboard
    2) Call up a list of different events they can reserve to
    3) Send reminders periodically.

    Events are held in memory and persisted at intervals with random tasks.
    Right now there are 3 distinct background tasks that run:

    1) task_dashboard_update
        Responsible for updating the dashboards when they are active in the discord.
        If a player chooses a reaction, this will edit the dashboard to display
        the results.
    2) task_auto_persist
        Responsible for persisting the reservations from memory into the postgres
        database at a set interval of time. This is to allow the website to be
        able to display (and take) reservations.
    3) task_reminder_alerts
        Responsible for sending alerts to a specified channel at predetermined
        intervals to remind someone about an upcoming event.
    """
    def __init__(self, bot: Bot):
        self.bot = bot
        self.dashboards: Dict[discord.Message, List[discord.Member]] = {}
        self.events: Dict[int, Any] = {}
        self.task_dashboard_update:Optional[asyncio.Task] = None
        self.task_auto_persist:Optional[asyncio.Task] = None
        self.task_reminder_alerts:Optional[asyncio.Task] = None

    def start_tasks(self):
        """
        Starts all background tasks if they are not already running.
        TODO: Heroku destroys the dynos and filesystem with this. This terminates
        the tasks and resets state.
        :return:
        """
        if self.task_not_running(self.task_dashboard_update):
            log.debug("Dashboard Task not running. Starting.")
            self.task_dashboard_update = self.bot.loop.create_task(self.dashboard_update())
            self.task_dashboard_update.add_done_callback(self.event_task_call_back)

        if self.task_not_running(self.task_auto_persist):
            log.debug("Auto Persist Task not running. Starting.")
            self.task_auto_persist = self.bot.loop.create_task(self.auto_persist())
            self.task_auto_persist.add_done_callback(self.event_task_call_back)
        if self.task_not_running(self.task_reminder_alerts):
            log.debug("Event Reminder Task not running. Starting.")
            self.task_reminder_alerts = self.bot.loop.create_task(self.reminder_alerts_task())
            self.task_reminder_alerts.add_done_callback(self.event_task_call_back)


    @staticmethod
    def get_field(embed: discord.Embed, name: str) -> Optional[str]:
        for field in embed.fields:
            if field.name == name:
                return field.value

    def is_member(self, event_id: int, player: discord.Member) -> bool:
        """
        Some events are tied to League Memberships. This checks if a member is
        a member of the league that is associated with the event.
        :param event_id: The event id in the database
        :param player: a Discord Member
        :return: True if member, False if not.
        """
        membership = self.events.get(event_id, {}).get('league', {}).get('membership', [])
        return player.id in [member['discord_id'] for member in membership]

    async def get_events(self, guild: discord.Guild) -> Optional[Dict[int, Any]]:
        """
        Gets all in-progress events and returns a sequence of dictionaries that have
            {
                'instance': The dictionary representation of the Event from the database
                'reservations': A list of Discord.Member objects that have 'reserved'. This is in memory.
                'dashboard': A Discord Message object that is the dashboard to update.
            }
        :param guild: The server guild
        :return: None
        """
        events = await restapi.get('event')
        if not events:
            return
        events = [e for e in events if e.get('status') == 'in progress']
        self.events = {}
        for e in events:
            id = int(e.get('id', 0))
            self.events[id] = {
                'instance': e,
                'reservations': self.get_reservations(guild, e),
                'dashboard': None,
            }
        return self.events

    def get_reservations(self, guild: discord.Guild, event: Dict[str, Any]) -> List[discord.Member]:
        """
        Populates Reservations from an Event instance to in memory Discord Player objects.
        The intent here is to move the persisted data over to local memory to allow us
        to track reservations in real-time without hitting the database.
        :param guild: The server guild
        :param event: The event 'instance'
        :return: A list of Discord Members that correspond to what is reserved in the database.
        """
        reservations = [get(guild.members, id=m.get('discord_id', -1))
                        for m in event.get('players_interested', [])]
        reservations = [r for r in reservations if r is not None]
        return reservations

    async def persist_reservations(self, event_id: int) -> Optional[Dict[str, Any]]:
        """
        Persists in-memory 'reservations' to the database
        :param event_id: the id number of the Event object in the database
        :return: Dictionary of updated instance of the Event object.
        """
        reservations = self.events.get(event_id, {}).get('reservations')
        if reservations is None or not isinstance(reservations, list):
            return
        reservations = [await get_or_create_site_member(p) for p in reservations]
        payload = {'id': event_id,
                   'players_interested': reservations
                   }
        event = await restapi.save('event', payload, partial=True)
        return event

    async def remove_dashboards(self, event_id: int) -> None:
        """
        Removes a dashboard that corresponds to a particular Event
        :param event_id: the id of the event dashboard to remove
        :return: None
        """
        dashboard = self.events.get(event_id, {}).get('dashboard')
        if isinstance(dashboard, discord.Message):
            try:
                await dashboard.delete()
            except discord.errors.NotFound as error:
                log.error(f"Tried to delete Event Dashboard that was not found: {error}")

    def find_by_dashboard(self, message_id: int) -> Optional[Dict[str, Any]]:
        """What a terrible hack"""
        for obj in self.events.values():
            dashboard = obj.get('dashboard')
            if isinstance(dashboard, discord.Message) and message_id == dashboard.id:
                return obj

    async def persist_and_remove_dashboards(self):
        """
        Persists all dashboards that are active and then removes the message.
        This is usually done when a user calls a command to display the dashboards.
        :return:
        """
        for id in self.events.keys():
            await self.persist_reservations(id)
            await self.remove_dashboards(id)

    async def create_dashboard(self, ch: discord.TextChannel, event_id: int) -> Optional[discord.Message]:
        """
        Create a dashboard for a particular event to display in a channel. A Dashboard is
        a tracked Embed object that is updated through a background task.
        :param ch: The Discord TextChannel to send to
        :param event_id: The id of the Event from the database
        :return: Discord Message object that is the dashboard.
        """
        obj = self.events.get(event_id)
        if not obj:
            return
        instance = obj.get('instance')
        await self.persist_reservations(event_id)
        await self.remove_dashboards(event_id)
        dashboard:discord.Message = await ch.send(embed=self.generate_embed(instance))
        await dashboard.add_reaction(constants.EMOJIS['CHECKIN'])
        self.events[event_id]['dashboard'] = dashboard

    async def create_dashboards(self, ctx: Context):
        """
        Creates all dashboards for an event
        :param ctx: Context
        :return: None
        """
        guild = ctx.guild
        log.debug(f"creating... events are {self.events}")
        if self.events:
            await self.persist_and_remove_dashboards()
        await self.get_events(guild)

        for id, results in self.events.items():
            instance = results.get('instance')
            reservations = self.get_reservations(guild, instance)
            results['reservations'] = reservations
            dashboard: discord.Message = await ctx.send(embed=self.generate_embed(instance))
            await dashboard.add_reaction(constants.EMOJIS['CHECKIN'])
            self.events[id]['dashboard'] = dashboard

    async def dashboard_update(self):
        """
        Actual Task that is run to update the dashboards.
        This will update a 'dashboard' object at a set interval by editing it,
        pulling in any updates to the fields of the instance.
        :return:
        """
        while True:
            await asyncio.sleep(2)
            if not self.events:
                continue
            for obj in self.events.values():
                instance = obj.get('instance')
                msg = obj.get('dashboard')
                if isinstance(msg, discord.Message):
                    try:
                        await msg.edit(embed=self.generate_embed(instance))
                    except discord.errors.NotFound:
                        # A command may have come through that caused the dashboards to
                        # clear. This will just silently fail until the next update
                        pass

    def generate_embed(self, event: Dict[str, Any]) -> discord.Embed:
        """
        Generates the updated Embed based on the information of an Event instance.
        :param event: The Event instance
        :return: A Discord Embed object
        """
        title = event.get('title', 'New Event')
        description = event.get('description', 'No Description')
        id = int(event.get('id'))

        host = event.get('host', {}).get('display_name', 'Not Specified')
        status = event.get('status', 'in progress')

        event_schedule = utils.normalize_date(event.get('event_schedule')) or "No Time"
        reservations = self.events[id].get('reservations', [])
        reserved = ', '.join(p.display_name for p in reservations) or "No Players"

        embed = utils.BrandedEmbed(title=title, description=description, dashboard=True)
        embed.add_field(name='id', value=str(id))
        embed.add_field(name='Host', value=host)
        embed.add_field(name='status', value=status)
        embed.add_field(name='Event Date/Time', value=f"{event_schedule} US/Central")
        embed.add_field(name='Players Reserved', value=reserved)
        embed.set_footer(text="To reserve for this event, click the 'Play' button")
        return embed

    @Cog.listener()
    async def on_reaction_add(self, reaction: discord.Reaction, user: discord.Member) -> None:
        """
        Tracks if a player reacts with an emoji designated to act as a 'Reserve' button.
        This handles how reservations are queued into the system.
        :param reaction: The reaction emoji
        :param user: The Discord Member that added the reaction.
        :return: None
        """
        obj = self.find_by_dashboard(reaction.message.id)
        if obj is None:
            return
        instance = obj.get('instance')
        reservations = obj.get('reservations')
        if user == self.bot.user or reservations is None or instance is None:
            return
        if reaction.emoji == constants.EMOJIS['CHECKIN']:
            if user not in reservations:
                reservations.append(user)
            log.debug(reservations)

    @Cog.listener()
    async def on_reaction_remove(self, reaction: discord.Reaction, user: discord.Member) -> None:
        """
        Tracks if a player removes an emoji designated to act as a 'Reserve' button.
        This handles how reservations are queued out of the system.
        :param reaction: The reaction that was removed
        :param user: The Discord Member that removed the reaction
        :return: None
        """
        obj = self.find_by_dashboard(reaction.message.id)
        if obj is None:
            return
        instance = obj.get('instance')
        reservations = obj.get('reservations')
        if user == self.bot.user or reservations is None or instance is None:
            log.debug(f"Reservations: {reservations} Event: {instance}")
            return
        if reaction.emoji == constants.EMOJIS['CHECKIN']:
            log.debug("Checking out")
            obj['reservations'] = [member for member in reservations if member != user]

    def task_not_running(self, task: Optional[asyncio.Task]) -> bool:
        return task is None or task.done()

    def event_task_call_back(self, future:asyncio.Future):
        log.debug(f"TASK COMPLETED CALLBACK Exception: {future.exception()} Events: {self.events}")


    @Cog.listener()
    async def on_ready(self):
        """
        Creates the tasks to run in the background and fetches the last persisted info from
        the database.
        :return: None
        """
        # Create different tasks
        # This is hard coded into one guild for now. We will need to change it on the database
        # to allow for multiple guilds.
        guild = self.bot.guilds[0]
        self.events = await self.get_events(guild)
        self.start_tasks()


    async def make_reminder_alert(self, event: Dict[str, Any], description=None):
        """
        Makes the reminder alert for a particular Event instance and displays it.
        :param event: The Event instance that contains the information.
        :param description: The Description of the reminder.
        :return:
        """
        for guild in self.bot.guilds:
            if not description:
                description = """Reminder that today we'll be hosting the event below. 
                If you plan to come, please use the dashboard below or, if its not there, type 
                !e info to retrieve it.
                """
            ch: discord.TextChannel = get(guild.text_channels, name='general')
            if not ch:
                return
            id = int(event.get('id', 0))
            title = event.get('title', 'New Event')
            embed = utils.BrandedEmbed(title=f"Reminder: {title}", description=description)
            embed.set_footer(text="To call up a list of events, type '!event info' in a bot command channel")
            await ch.send(embed=embed)
            await self.create_dashboard(ch, id)


    async def reminder_alerts_task(self):
        """
        Task that sends periodic reminders about an upcoming Event.
        :return: None
        """
        while True:
            await asyncio.sleep(60) # Every minute
            if not self.events:
                continue

            for obj in self.events.values():
                instance = obj.get('instance', {})
                schedule:dt = utils.to_datetime(instance.get('event_schedule'))
                str_date = schedule.strftime("%A at %I:%M%p US/CENTRAL")
                # Early Reminder about event that day
                now = dt.now(timezone.utc)
                time_remaining = schedule - now
                if time_remaining < timedelta(days=0, minutes=0):
                    continue
                if timedelta(days=1, minutes=0) < time_remaining < timedelta(days=1, minutes=1):
                    description = f"This event is TOMMOROW! {str_date}"
                    await self.make_reminder_alert(instance, description)

                if timedelta(days=0, minutes=58) < time_remaining < timedelta(days=0, minutes=59):
                    description = f"The event starts in ONE HOUR. Finish your games now and report to the lobby!"
                    await self.make_reminder_alert(instance, description)


    async def auto_persist(self):
        """
        Task that persists all active dashboards at every set interval.
        This is to allow the website to relatively stay up to date realtime.
        :return:
        """
        while True:
            # Every 10 minutes
            await asyncio.sleep(10*60)
            if self.events:
                for id in self.events.keys():
                    await self.persist_reservations(id)

    # Commands
    @commands.group('event', aliases=('e',))
    async def event_rw_group(self, ctx: Context) -> None:
        pass

    """
    MODERATION HELPER COMMANDS
    """

    @with_role(constants.ROLES['ADMIN'])
    @event_rw_group.command(name='completeall')
    async def event_mod_completeall(self, ctx: Context):
        events = await restapi.get('event')
        admin_channel = utils.get_mod_log_channel(ctx.guild)
        if not events or not admin_channel:
            await ctx.send("Could not complete command. No Events or log channel undefined", delete_after=10)
            return
        for e in events:
            if e.get('status') == 'in progress':
                id = e.get('id')
                title = e.get('title')
                e['status'] = 'completed'
                await restapi.save('event', e)
                await admin_channel.send(f"Completed Event #{id} - {title}")

    @with_role(constants.ROLES['ADMIN'])
    @event_rw_group.command(name='createtraining')
    async def event_mod_createtraining(self, ctx: Context):
        admin_channel = utils.get_mod_log_channel(ctx.guild)
        if not admin_channel:
            await ctx.send("Could not complete command. Log channel undefined", delete_after=10)
            return
        host = await restapi.get('discordplayer', ctx.author.id)
        tuesday = {
            'title': 'Oblivion Training Night',
            'description': 'Join us on another session where we work on a specific aspect of the game. Open to all players',
            'event_schedule': str(utils.get_next_day(weekday=2, hour=20)),
            'host': host,
            'status': 'in progress',
        }
        result = await restapi.save('event', tuesday)
        await admin_channel.send(f"Created event #{result.get('id')} {result.get('title')}")

    @with_role(constants.ROLES['ADMIN'])
    @event_rw_group.command(name='createintermediate')
    async def event_mod_createintermediate(self, ctx: Context):
        admin_channel = utils.get_mod_log_channel(ctx.guild)
        if not admin_channel:
            await ctx.send("Could not complete command. Log channel undefined", delete_after=10)
            return
        host = await restapi.get('discordplayer', ctx.author.id)
        friday = {
            'title': 'Intermediate League Night',
            'description': 'Come join us for another Intermediate league night. Games start at 7pm US/Central time. Interested in joining the league? See a Contributor',
            'league': await restapi.get('league', resource_id=3),
            'event_schedule': str(utils.get_next_day(weekday=4, hour=19)),
            'host': host,
            'status': 'in progress',
        }
        result = await restapi.save('event', friday)
        await admin_channel.send(f"Created event #{result.get('id')} {result.get('title')}")

    @with_role(constants.ROLES['ADMIN'])
    @event_rw_group.command(name='createpro')
    async def event_mod_createpro(self, ctx: Context):
        admin_channel = utils.get_mod_log_channel(ctx.guild)
        if not admin_channel:
            await ctx.send("Could not complete command. Log channel undefined", delete_after=10)
            return
        host = await restapi.get('discordplayer', ctx.author.id)
        sat = {
            'title': 'Professional League Night',
            'description': 'Come join us for another Professional league night. Games start at 8pm US/Central time. Interested in joining the league? See a Contributor',
            'league': await restapi.get('league', resource_id=1),
            'event_schedule': str(utils.get_next_day(weekday=5, hour=20)),
            'host': host,
            'status': 'in progress',
        }
        result = await restapi.save('event', sat)
        await admin_channel.send(f"Created event #{result.get('id')} {result.get('title')}")

    @with_role(constants.ROLES['ADMIN'])
    @event_rw_group.command(name='createce')
    async def event_mod_createce(self, ctx: Context):
        admin_channel = utils.get_mod_log_channel(ctx.guild)
        if not admin_channel:
            await ctx.send("Could not complete command. Log channel undefined", delete_after=10)
            return
        host = await restapi.get('discordplayer', ctx.author.id)
        sat = {
            'title': 'CE Tournament',
            'description': 'Come join us for another CE Tournament night. Games start at 3pm US/Central time. Open to all players.',
            'league': await restapi.get('league', resource_id=1),
            'event_schedule': str(utils.get_next_day(weekday=5, hour=15)),
            'host': host,
            'status': 'in progress',
        }
        result = await restapi.save('event', sat)
        await admin_channel.send(f"Created event #{result.get('id')} {result.get('title')}")


    @with_role(constants.ROLES['ADMIN'])
    @event_rw_group.command(name='createofficial')
    async def event_mod_createofficial(self, ctx: Context):
        #await ctx.invoke(self.event_mod_createtraining)
        await ctx.invoke(self.event_mod_createintermediate)
        await ctx.invoke(self.event_mod_createce)
        await ctx.invoke(self.event_mod_createpro)

    @event_rw_group.command(name='info')
    async def event_info(self, ctx: Context):
        """
        Calls up a list of events to a text channel.
        :param ctx: Context
        :return: None
        """
        await ctx.send("Hold on while I fetch this information...", delete_after=5)
        await ctx.trigger_typing()
        await self.create_dashboards(ctx)

    @with_role(constants.ROLES['ADMIN'])
    @event_rw_group.command(name='complete')
    async def event_complete(self, ctx: Context, event_id: int):
        event = await restapi.get('event', event_id)
        if event:
            event['status'] = 'completed'
            await restapi.save('event', event)
            await ctx.send(f"Event #{event.get('id')} {event.get('title')}: Completed", delete_after=10)
        else:
            await ctx.send(f"Could Not locate Event #{event_id}", delete_after=10)

def setup(bot: Bot):
    """Responsible for loading the MatchCOG extension into discord.py"""
    bot.add_cog(EventCOG(bot))
    log.info("Cog loaded: EventCOG (Rewrite)")