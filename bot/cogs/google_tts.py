import asyncio
import base64
import io
import logging
import os
from typing import Optional

import discord
from aiogoogletrans import Translator
from discord import PCMAudio
from discord.ext.commands import Bot, Context, command, Cog

from bot import constants
from bot import restapi
from bot.decorators import with_role
from discord.ext import tasks

log = logging.getLogger(__name__)

class VoiceItem:
    """
    Acts as a container for a Voice command. This is added to a async queue.
    """
    def __init__(self, ch: discord.VoiceChannel, message: str, *,
                 dialect: Optional[str] = None, **kwargs):
        self.voice_channel = ch
        self.message = message
        self.dialect = dialect
        self.kwargs = kwargs


class GoogleTTSCOG(Cog):
    """
    This COG is responsible for taking the Google TTS API and converting it
    to a format that you can stream in Discord. Since the bot can only be connected
    to one channel at a time, it manages the requests into queues.
    """

    async def google_voice_task(self, queue: asyncio.Queue):
        """
        Constantly checks if a new item enters the queue and voices it.
        :param queue: The asyncio queue to use.
        :return: None
        """
        log.debug("STARTING TTS TASK!")
        while True:
            log.debug('waiting to play')
            await self.can_play.wait()
            broadcast:VoiceItem = await queue.get()
            self.can_play.clear()
            log.debug('playing')
            vcl:discord.VoiceClient = await broadcast.voice_channel.connect()
            if broadcast.dialect:
                source = await GoogleTTS.as_source(broadcast.message, dialect=broadcast.dialect, **broadcast.kwargs)
            else:
                source = await GoogleTTS.as_source(broadcast.message, **broadcast.kwargs)
            vcl.play(source, after=lambda e: self.afterwards_disconnect(vcl, queue))

    def __init__(self, bot: Bot):
        self.bot = bot
        self.queue:asyncio.Queue = asyncio.Queue()
        self.worker_task:asyncio.Task = self.bot.loop.create_task(self.google_voice_task(self.queue))
        self.worker_task.add_done_callback(self.voice_callback)
        self.can_play = asyncio.Event()
        self.can_play.set()

    def voice_callback(self, task:asyncio.Task):
        if task.exception():
            log.debug(task.print_stack())
            log.debug('Exception found')

    def afterwards_disconnect(self, vcl: discord.VoiceClient, queue: asyncio.Queue):
        """
        A Callback function designed to disconnect the bot from the voice channel
        after it has finished voicing the request.
        :param vcl:
        :param queue:
        :return:
        """
        coro = vcl.disconnect()
        fut = asyncio.run_coroutine_threadsafe(coro, self.bot.loop)
        try:
            fut.result()
            queue.task_done()
            self.bot.loop.call_soon_threadsafe(self.can_play.set)
        except Exception as e:
            raise

    async def speak(self, voice_channel: discord.VoiceChannel, message: str, **kwargs):
        """
        Helper function that adds a VoiceItem to the queue.
        :param voice_channel: The Discord Voice Channel to voice to
        :param message: The Text Message to convert to speech
        :param kwargs: Any other options (ssml, etc)
        :return: None
        """
        log.debug('speaking')
        broadcast = VoiceItem(voice_channel, message, **kwargs)
        await self.queue.put(broadcast)
        log.debug(self.queue)


    @with_role(constants.ROLES['ADMIN'])
    @command(name='voice', aliases=('v',))
    async def googletts_voice(self, ctx: Context, voice_channel: discord.VoiceChannel, *, message):
        """
        Command to have the bot voice a particular message to a channel.
        :param ctx: Context
        :param voice_channel: The channel name to voice to
        :param message: The message to voice
        :return:
        """
        broadcast = VoiceItem(voice_channel, message)
        await self.queue.put(broadcast)


    @with_role(constants.ROLES['ADMIN'])
    @command(name='voicetr', aliases=('vtr',))
    async def googletts_voice_translate(self, ctx: Context, lang: str,
                                        voice_channel: discord.VoiceChannel, *, message):
        tr = Translator()
        translation = await tr.translate(message, dest=lang)
        broadcast = VoiceItem(voice_channel, translation.text, dialect=lang)
        await self.queue.put(broadcast)


class GoogleTTS:
    """
    Google TTS Implementation. This takes the Google TTS service and makes the api call
    based on a message. It also handles the conversion to PCMAudio for discord.
    TODO: This needs massive refactoring.
    """
    @classmethod
    async def as_source(cls, text: str, **kwargs) -> PCMAudio:
        """
        Returns a PCMAudio Source that Discord can use to stream voice data.
        :param text: The message to encode to PCMAudio
        :param kwargs: Any additional flags
        :return: PCMAudio Frames
        """
        self = GoogleTTS(text)
        await self.synthesize(**kwargs)
        return PCMAudio(self.audio_stream)


    def __init__(self, text):
        self.text = text
        self.audio_stream:io.BytesIO = None

    async def get_language(self, lang: str):
        """
        Gets a list of 'voices' from a particular language to give the bot a reasonable
        accent for any language that it speaks in.
        Currently uses all female voices.
        :param lang: the language code to lookup (ex: 'es' for Espanol)
        :return: Tuple of languages
        """
        google_api_key = os.environ.get('GOOGLE_API_TOKEN')
        target = f"https://texttospeech.googleapis.com/v1/voices?key={google_api_key}&languageCode={lang}"
        status, resp = await restapi.api_method('get', target=target)
        if resp.get('voices'):
            lang = sorted([l for l in resp['voices'] if l['ssmlGender'] == 'FEMALE'], key=lambda k: k['name'], reverse=True)[0]
            return lang['languageCodes'][0], lang['name']
        else:
            return "en-US", "en-US-Wavenet-F"

    async def synthesize(self, languageCode='en-US', name='en-US-Wavenet-F', dialect=None, ssml=False):
        """
        Makes the API call and gets the audio stream from the request.
        :param languageCode: THe languageCode to use
        :param name: Tje default Wavenet Voice or custom
        :param dialect: What dialoext or Language
        :param ssml: Boolean if this is speech-formatted text or regular.
        :return: None
        """
        if dialect:
            languageCode, name = await self.get_language(dialect)

        google_api_key = os.environ.get('GOOGLE_API_TOKEN')
        target = f"https://texttospeech.googleapis.com/v1/text:synthesize?key={google_api_key}"
        payload = {
                      "audioConfig": {
                        "audioEncoding": "LINEAR16",
                        "sampleRateHertz": 96000,
                        "speakingRate": 1.0,
                      },
                      "input": {

                      },
                      "voice": {
                        "languageCode": languageCode,
                        "name": name,
                      }
                }
        input_type = 'ssml' if ssml else 'text'
        payload['input'][input_type] = self.text
        status, resp = await restapi.api_method('post', target=target, json=payload)
        if resp.get('audioContent'):
            self.audio_stream = io.BytesIO(base64.b64decode(resp['audioContent']))

def setup(bot: Bot):
    """Responsible for loading the GoogleTTSCOG extension into discord.py"""
    bot.add_cog(GoogleTTSCOG(bot))
    log.info("Cog loaded: GoogleTTSCOG")