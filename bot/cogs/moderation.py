import asyncio
import logging
import os
import pickle
import re
from collections import Counter
from collections import defaultdict
from datetime import timedelta, datetime
from typing import List
import json
import discord
import pytz
from discord import Embed
from discord.ext import commands
from discord.ext.commands import command, Context, Bot, Cog, group
from discord.ext.commands.converter import Greedy
from discord.utils import get

from bot import constants
from bot import restapi
from bot import utils
from bot import webapi
from bot.converters import MessageConverter
from bot.converters import TimeDelta
from bot.decorators import with_role
from bot.pagination import LinePaginator
import string


log = logging.getLogger(__name__)

FILE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..', 'static'))
PATH = os.path.normpath(f'{FILE_DIR}/moderation_schedules.pickle')

async def send_to_mod_channels(ctx, **kwargs):
    for ch in constants.MODERATION_CHANNELS:
        mod_channel = get(ctx.guild.channels, name=ch)
        if mod_channel:
            await mod_channel.send(**kwargs)
        else:
            log.error(f"Undefined channel in MODERATION_CHANNELS: {ch}")


def dt_string(dt):
    return (
        dt
            .astimezone(pytz.timezone('US/Central'))
            .strftime("%D - %I:%M%p US/Central")
    )


def make_timedelta(last_active: str) -> timedelta:
    """
    Format is
    1w2d1h18m2s

    :param last_active:
    :return:
    """
    pattern = '^(?:(?P<weeks>\d+)[w.])?(?:(?P<days>\d+)[d.])?(?:(?P<hours>\d+)[h.])?(?:(?P<minutes>\d+)[m.])?(?:(?P<seconds>\d+)[s.])?$'
    pattern = re.compile(pattern)
    matches = re.search(pattern, last_active)
    if matches is None:
        return timedelta(weeks=0, days=0, hours=0, minutes=0, seconds=0)

    args = {k: int(v) for k, v in matches.groupdict().items() if v and v.isdigit()}
    return timedelta(**args)


class Moderation(webapi.WebAPIMixin, Cog):
    """Moderation commands"""

    def __init__(self, bot: Bot):
        self.bot: Bot = bot
        self.guild = None
        self.muted_role = None
        self._muted_all = []
        self.schedules = defaultdict(dict)
        self.task_schedules = None
        self.ez_words = []
        self.ez_msg = {}
        self.last_msg_ez: discord.Message = None
    """
    async def mute_players(self, players: List[discord.Member], reason: str):
        for p in players:
            await p.edit(mute=True, reason=reason)

    async def unmute_players(self, players: List[discord.Member], reason: str):
        for p in players:
            await p.edit(mute=False, reason=reason)
    """

    def commit_schedules_to_disk(self):
        with open(PATH, 'wb') as f:
            pickle.dump(self.schedules, f)

    async def ez(self, message: discord.Message, reason=None):
        ctx = await self.bot.get_context(message)
        id = message.author.id
        name = message.author.display_name
        reason = reason if reason else f"{name} said ez. Who's ez now? MUTED for 2 mins!"
        delta = utils.twitch_to_time_delta("2m")
        embed = discord.Embed(
            title="EZ Banned",
            description=reason
        )
        await ctx.invoke(
            self.mute_players,
            players=[message.author],
            delta=delta,
            reason=reason
        )
        await ctx.send(embed=embed, delete_after=5)
        await message.delete()

    @Cog.listener()
    async def on_message_edit(self, before, after):
        await self.on_message(after)

    """
    @Cog.listener()
    async def on_raw_reaction_add(self, reaction: discord.RawReactionActionEvent):
        e = reaction.emoji.__str__()
        ch:discord.TextChannel = self.bot.get_channel(reaction.channel_id)
        user = self.bot.get_user(reaction.user_id)
        message: discord.Message = await ch.fetch_message(reaction.message_id)
        guild = message.guild
        member = discord.utils.get(guild.members, id=user.id)
        ctx = await self.bot.get_context(message)
        if user is None or message is None or member is None:
            return

        if e == '\U0001F1EA' or e == '\U0001F1FF':
            reason = f"{member.display_name} used a E or Z reaction."
            embed = discord.Embed(
                title="EZ Banned",
                description=reason
            )
            await message.remove_reaction(e, user)
            await ctx.invoke(
                self.mute_players,
                players=[member],
                delta=utils.twitch_to_time_delta("2m"),
                reason=reason
            )
            await ctx.send(embed=embed, delete_after=5)
    """
    @Cog.listener()
    async def on_message(self, message: discord.Message):
        id = message.author.id
        name = message.author.display_name

        if id in self.schedules.get('pb', []):
            if message.mentions or message.role_mentions:
                ctx = await self.bot.get_context(message)
                reason = f"{name} IS PING BANNED. Muted for 1 hour."
                delta = utils.twitch_to_time_delta("1h")
                embed = discord.Embed(
                    title="Ping Banned",
                    description=reason
                )
                await ctx.invoke(
                    self.mute_players,
                    players=[message.author],
                    delta=delta,
                    reason=reason
                )
                await ctx.send(embed=embed, delete_after=300)
        """
        simple_words = ['ez', 'easy', 'eazy', 'eaz', 'eesy']
        if len(self.ez_msg.get(str(id), '')) >= 5:
            del self.ez_msg[str(id)]
        
        word = ""
        cleaned_message = utils.remove_accents(utils.replace_regional(message.clean_content))
        if isinstance(self.last_msg_ez, discord.Message):
            last_msg_cleaned = utils.remove_accents(utils.replace_regional(self.last_msg_ez.clean_content))
            try:
                last_msg = re.sub(r'[_\s]|[^\w\s]', '', last_msg_cleaned.split()[-1])[-1]
                this_msg = re.sub(r'[_\s]|[^\w\s]', '', cleaned_message.split()[-1])[-1]
                last_msg = last_msg + this_msg
            except IndexError:
                last_msg = ''
        else:
            last_msg = ''
        try:

            self.ez_msg[str(id)] = self.ez_msg.get(str(id), '') + re.sub(r'[_\s]|[^\w\s]','', cleaned_message.split()[-1])[-1]
        except IndexError:
            pass

        for w in cleaned_message.split():
            word += re.sub(r'[_\s]|[^\w\s]','', w)


            if any(e in word.lower() for e in simple_words) and w.lower() not in self.ez_words:
                log.error("1")
                await self.ez(message)
                return
            if 'ez' in w.lower() and w.lower() not in self.ez_words:
                await self.ez(message)
        if any(e in self.ez_msg.get(str(id), '').lower() for e in simple_words):
            del self.ez_msg[str(id)]
            await self.ez(message)
            log.error("2")

        if len(cleaned_message.split('\n')) >= 3 and cleaned_message.count('|') >= 3:
            if self.ez_msg.get(str(id)):
                del self.ez_msg[str(id)]
            await self.ez(message)
            log.error("3")
        if any(e in last_msg.lower() for e in simple_words):
            await self.ez(self.last_msg_ez)
            await self.ez(message)
            log.error("4")
        else:
            self.last_msg_ez = message
        """

    async def moderation_loop(self):
        while True:
            log.debug("Moderation Loop looking for expirations")
            for player_id, expiration in list(self.schedules.get('muted', {}).items()):
                now = utils.utc_now()
                if now >= expiration:
                    player = get(self.guild.members, id=player_id)
                    if player:
                        title = f"{player.display_name} Mute removed"
                        description = f"Expired at {dt_string(expiration)}"
                        await player.remove_roles(self.muted_role)
                        await utils.send_to_log_channel(self.guild,
                                                         embed=discord.Embed(title=title, description=description)
                                                         )
                        log.info(f"Removing Muted Role from {player.display_name}")
                    del self.schedules['muted'][player_id]
                    self.commit_schedules_to_disk()
            await asyncio.sleep(60)

    def moderation_task_finished(self, task: asyncio.Task):
        if task.exception():
            log.error("Exception found in Moderation Task. Task has terminated")
            task.print_stack()


    @commands.command(name='ping-ban', aliases=('pb',))
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def ping_ban(self, ctx: Context, players: Greedy[discord.Member] = []):
        pbs = self.schedules.get('pb')
        if pbs is None:
            pbs = []
            self.schedules['pb'] = pbs

        if len(players) <= 0:
            users = [get(ctx.guild.members, id=id) for id in self.schedules['pb']]
            users = [u for u in users if u is not None]
            users = '\n'.join(u.display_name for u in users)
            embed = discord.Embed(
                title="Ping bans",
                description=users or utils.EMPTYLINE
            )
            await ctx.send(embed=embed, delete_after=20)
            return
        for p in players:
            if p.id in pbs:
                self.schedules['pb'] = [pb for pb in self.schedules['pb'] if p.id != pb]
            else:
                self.schedules['pb'].append(p.id)
        self.commit_schedules_to_disk()



    @commands.command(name='temp-mute', aliases=('tm',))
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def mute_players(self, ctx: Context, players: Greedy[discord.Member], delta: TimeDelta, *, reason: str = "No Reason"):
        site_author = await restapi.get('discordplayer', ctx.author.id)
        for p in players:
            site_player = await restapi.get('discordplayer', p.id)
            name = p.display_name
            expiration = utils.utc_now() + delta
            self.schedules['muted'][p.id] = expiration
            await p.add_roles(self.muted_role)
            delta_str = utils.timedelta_condensed(delta)
            title = f"MOD {ctx.author.display_name} MUTED {name} FOR {delta_str}"
            description = f"{reason}\n *Expires* {dt_string(expiration)}"
            if p.voice:
                await p.move_to(ctx.guild.afk_channel, reason=f"{title}: {description}")

            await ctx.invoke(self.infraction_add, p, reason=f"MUTED until {dt_string(expiration)}: {reason}")

        self.commit_schedules_to_disk()

    @commands.command(name='unmute', aliases=('um',))
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def unmute_players(self, ctx: Context, players: Greedy[discord.Member], *, reason: str):
        for p in players:
            try:
                del self.schedules['muted'][p.id]
                await p.remove_roles(self.muted_role)
                title = f"MOD {ctx.author} UN-MUTED {name}"
                description = reason or "No Reason"
                embed = utils.BrandedEmbed(title=title, description=description)
                await utils.send_to_log_channel(self.guild, embed=embed)
                await ctx.send(embed=embed, delete_after=10)
                await p.send(embed=embed)

            except (KeyError, discord.errors.Forbidden):
                pass
        self.commit_schedules_to_disk()

    @command(name='muteall', aliases=('ma',))
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def mod_mute_all(self, ctx: Context):
        await ctx.trigger_typing()
        ch = ctx.author.voice.channel
        reason = f"Muted all users in Voice channel {ch.name}"
        for p in ch.members:
            if utils.has_any_role(p, constants.ROLES['MODERATORS']):
                continue
            if p not in self._muted_all:
                self._muted_all.append(p)
                await p.edit(mute=True, reason=reason)
        await ctx.send(f"{ch.name} members server-muted", delete_after=10)


    @command(name='unmuteall', aliases=('uma',))
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def mod_unmute_all(self, ctx: Context):
        reason = "Un muted all members previously muted in voice channel."
        await ctx.trigger_typing()
        for p in self._muted_all:
            await p.edit(mute=False, reason=reason)
        await ctx.send("Voice mute removed", delete_after=10)
        self._muted_all = []

    @group(name='dm', invoke_without_command=True)
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def mod_dm_group(self, ctx: Context, player: discord.Member, num: int = 1, *, reason: str = "No Reason"):
        msgs = []
        async for msg in ctx.channel.history(limit=100):
            if len(msgs) >= num:
                break
            if msg.author == player:
                msgs.append(msg)
        num_messages = len(msgs)
        await ctx.channel.delete_messages(msgs)
        name = player.display_name
        reason = f"Removed {num_messages} msgs from #{ctx.channel.name}: {reason}"
        await ctx.invoke(self.infraction_add, player, reason=reason)

    @commands.command(name='warn')
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def mod_warn(self, ctx: Context, players: Greedy[discord.Member], *, reason: str = "No Reason"):
        for p in players:
            await ctx.invoke(self.infraction_add, p, reason=f"WARNED: {reason}")

    @mod_dm_group.command(name='after')
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def mod_delete_after_message(self, ctx: Context, msg: MessageConverter, *, reason: str = "No Reason"):
        check = lambda m: m.author == msg.author
        player = msg.author
        msgs = await ctx.channel.purge(check=check, after=msg)
        await msg.delete()
        num_messages = len(msgs) + 1
        reason = f"Removed {num_messages} msgs from #{ctx.channel.name}: {reason}"
        await ctx.invoke(self.infraction_add, player, reason=reason)

    @mod_dm_group.command(name='all-after')
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def mod_delete_all_after_message(self, ctx: Context, msg: MessageConverter, *, reason: str = "No Reason"):
        msgs = await ctx.channel.purge(after=msg)
        await msg.delete()
        mod = ctx.author.display_name
        title = "Moderation Action"
        description = f"{mod} DELETED {len(msgs) +1} msgs from #{ctx.channel.name}\n\n{reason}"
        embed = discord.Embed(title=title, description=description)
        await utils.send_to_log_channel(ctx.guild, embed=embed)

    @mod_dm_group.command(name='all-between', aliases=('ab',))
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def mod_delete_between_message(self, ctx: Context, after_msg: MessageConverter, before_msg: MessageConverter,
                                         *, reason: str ="No Reason"):
        msgs = await ctx.channel.purge(after=after_msg, before=before_msg)
        try:
            await after_msg.delete()
            await before_msg.delete()
        except discord.errors.NotFound:
            pass

        mod = ctx.author.display_name
        title = "Moderation Action"
        description = f"{mod} DELETED {len(msgs) +1} msgs from #{ctx.channel.name}\n\n{reason}"
        embed = discord.Embed(title=title, description=description)
        await utils.send_to_log_channel(ctx.guild, embed=embed)

    @mod_dm_group.command(name='between')
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def mod_delete_between_user(self, ctx: Context, after_msg: MessageConverter,
                                      before_msg: MessageConverter, *, reason: str = "No Reason"):
        player = before_msg.author
        check = lambda m: m.author == player
        msgs = await ctx.channel.purge(after=after_msg, before=before_msg, check=check)
        try:
            await after_msg.delete()
            await before_msg.delete()
        except discord.errors.NotFound:
            pass

        mod = ctx.author.display_name
        title = "Moderation Action"
        description = f"Removed {len(msgs) + 2} msgs from #{ctx.channel.name}\n\n{reason}"
        await ctx.invoke(self.infraction_add, player, reason=description)

    @mod_dm_group.command(name='msg')
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def mod_delete_specific(self, ctx: Context, msg: MessageConverter, *, reason: str = "No Reason"):
        player = msg.author
        try:
            await msg.delete()
        except discord.errors.NotFound:
            pass
        description = f"Removed msg from #{ctx.channel.name}\n\n{reason}"
        await ctx.invoke(self.infraction_add, player, reason=description)

    @commands.command(name='kick')
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def mod_kick(self, ctx: Context, players: Greedy[discord.Member], *, reason: str = "No Reason"):
        guild:discord.Guild = ctx.guild
        description = f"Kicked from ZC Leagues Server.\n\n{reason}"
        for p in players:
            await ctx.invoke(self.infraction_add, p, reason=description)
            await guild.kick(p, reason=reason)

    @with_role(constants.ROLES['ADMIN'])
    @command(name='leagues')
    async def league_count(self, ctx: Context):
        roles = ['Professional League', 'Temp Pro League', 'Intermediate League',
                 'Temp Intermediate', 'Up and Coming League']
        embed = utils.BrandedEmbed(title="Role Counts")
        for role in roles:
            role = get(ctx.guild.roles, name=role)
            players = '\n'.join(p.display_name for p in role.members)
            embed.add_field(name=f"{role.name} - {len(role.members)}", value=players)
        await ctx.send(embed=embed)



    @group('infraction', aliases=('inf',))
    async def inf_group(self, ctx: Context):
        pass

    @with_role(constants.ROLES['ADMIN'])
    @inf_group.command(name='add')
    async def infraction_add(self, ctx: Context, player: discord.Member,
                             *, reason: str):
        site_player = await self.get_record('discordplayer', player.id)
        author = await self.get_record('discordplayer', ctx.author.id)
        if author:
            infraction = {
                'player': site_player,
                'author': author,
                'reason': reason
            }
            instance = await self.api_save('infraction', infraction)

            embed = Embed(title='Moderation Action')
            embed.add_field(name='Player', value=player.display_name)
            embed.add_field(name='Moderator', value=ctx.author.display_name)
            embed.add_field(name='Date/Time',
                            value=self.normalize_date(instance['created']))
            embed.add_field(name='Info', value=reason, inline=False)
            try:
                await utils.send_to_log_channel(ctx.guild, embed=embed)
                await player.send(embed=embed)
            except discord.errors.Forbidden:
                pass

    @inf_group.command(name='info')
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def infraction_info(self, ctx: Context, player: discord.Member = None):
        if player is None:
            infractions = await self.all_records('infraction')
            counts = Counter(i['player']['display_name'] for i in infractions).most_common()
            embed = Embed(title='Infraction counts by user.')
            await LinePaginator.paginate(
                lines=(f"{i[0]}: {i[1]}" for i in counts),
                ctx=ctx,
                embed=embed,
                empty=False,
                max_lines=constants.PAGINATION_MAX_LINES,
            )
        else:
            results = ()
            infractions = await self.all_records('infraction', discordid=player.id)
            # infractions = infractions.sort(key=itemgetter('created'), reverse=True)
            for i in infractions:
                inf_date = utils.normalize_date(i['created'])
                results = results + (f"{inf_date} By {i['author']['display_name']}: "
                                     f"{i['reason']}",)
            log.info(results)
            embed = Embed(title=f"Infraction information for {player.name}")
            await LinePaginator.paginate(
                lines=results,
                ctx=ctx,
                embed=embed,
                empty=False,
                max_lines=constants.PAGINATION_MAX_LINES
            )

    @commands.command(name='ww')
    async def grant_ww(self, ctx: Context):
        role = get(ctx.guild.roles, name='ww')
        player: discord.Member = ctx.author
        if role is None:
            role = await utils.get_or_create_role(ctx.guild, name='ww', mentionable=False)
        if role in player.roles:
            await player.remove_roles(role)
        else:
            await player.add_roles(role)
        await utils.reaction(True, ctx)

    @Cog.listener()
    async def on_ready(self):
        self.guild = self.bot.guilds[0]
        self.muted_role = await utils.get_or_create_role(self.guild, name='Muted', mentionable=False)
        log.info("Loading Moderation Schedules and Launching Moderation Task")
        try:
            with open(PATH, 'rb') as f:
                # self.schedules = json.load(f, object_hook=datetime_parser)
                # log.debug(self.schedules)
                self.schedules = pickle.load(f)
        except FileNotFoundError:
            pass

        try:
            with open(f"{FILE_DIR}/ez.json", "r") as f:
                self.ez_words = json.load(f)
        except FileNotFoundError:
            pass

        self.task_schedules = self.bot.loop.create_task(self.moderation_loop())
        self.task_schedules.add_done_callback(self.moderation_task_finished)

    @Cog.listener()
    async def on_command_completion(self, ctx: Context):
        if ctx.cog == self:
            try:
                await ctx.message.add_reaction(constants.EMOJIS['SUCCESS'])
                await asyncio.sleep(5)
                await ctx.message.delete()
            except discord.errors.NotFound:
                pass

"""
    @Cog.listener()
    async def on_command_error(self, ctx: Context, exception):
        if ctx.cog == self:
            try:
                await ctx.message.add_reaction(constants.EMOJIS['ERROR'])
            except discord.errors.NotFound:
                pass
            msg = f"'{ctx.author.name}' ran command '{ctx.command}' and fails with '{exception}'"
            log.error(msg)
            await asyncio.sleep(5)
            await ctx.message.delete()
            raise exception
"""
def setup(bot: Bot):
    bot.add_cog(Moderation(bot))
    log.info("Cog loaded: Moderation")
