import asyncio
# from bot.HostSession im, safe_value
import copy
import json
import logging
import os
import random
import typing

import dill
import discord
import websockets
from discord.ext import commands
from discord.ext import tasks
from discord.ext.commands import Context, Bot
from discord.ext.commands import Greedy

from bot import constants
from bot import restapi
from bot import utils
from bot.HostSession import HostSession, safe_value
from bot.cogs.google_tts import GoogleTTSCOG
import collections
import twitchio
from twitchio.dataclasses import Channel as TwitchChannel
import textwrap

log = logging.getLogger(__name__)
FILE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..', 'static'))
PATH = os.path.normpath(f'{FILE_DIR}/host_sessions.pickle')
COG_CONFIG = os.path.normpath(f'{FILE_DIR}/queue_config.json')

class Queues(commands.Cog):
    """COG responsible for providing all functionality regarding match queues

    Commands
    -----------
    """
    def __init__(self, bot: Bot):
        """Initializes the Queue COG

        This sets up some default containers, one being hosts, and the other
        being written hosts. This is handled by the persisting task which
        also starts once the COG is initialized to track state changes and
        commit to disk.

        Parameters
        ----------
        bot
            The bot instance
        """
        self.bot = bot
        self.hosts: typing.Dict[str, HostSession] = {}
        self.config = collections.defaultdict(dict)

        # TODO: GIL lock freezes up HostSession tasks. Need to fix.
        # See https://stackoverflow.com/questions/43310306/python-script-gets-stuck-on-dill-dump
        #
        self.written_hosts: typing.Dict[str, HostSession] = {}
        #self.persisting_task.start()

    def __del__(self):
        #self.persisting_task.cancel()
        for hs in self.hosts.values():
            del hs
        del self.hosts

    @commands.Cog.listener()
    async def on_ready(self):
        try:
            with open(COG_CONFIG, 'r') as f:
                self.config.update(json.load(f))
        except FileNotFoundError:
            pass
        log.debug(f"{self.config}")

    def fallback_channel(self, guild: discord.Guild) -> typing.Optional[discord.VoiceChannel]:
        guild_string = str(guild.id)
        channel = self.config.get(guild_string, {}).get('fallback_channel')
        if channel is not None:
            channel = discord.utils.get(guild.voice_channels, id=channel)
        return channel


    def cog_unload(self):
        """Safely clean up the COG when unloading.

        This stops all websockets from listening to the zcleagues server
        and gracefully cancels all the tasks for any HostSession that exists.

        Returns
        -------
        None
        """
        #self.persisting_task.cancel()
        for hs in self.hosts.values():
            hs.stop_tasks()

    def get_host(self, member: typing.Union[int, discord.Member]) -> HostSession:
        """Gets a HostSession for a given member.

        This can accept either a discord member or an integer id.

        ..note::
            All keys in host are a string representation of the integer id.

        Parameters
        ----------
        member: Union[:class:`int`, :class:`discord.Member`]

        Returns
        -------
        :class:`HostSession`

        """
        key = safe_value(member)
        host = self.hosts.get(str(key))
        if host is None:
            log.debug(f"No host found: {key}: {self.hosts}")
        return host

    def state_has_changed(self) -> bool:
        """Helper function to detect the hosts container has changed.

        Not the best way to do this, but a carbon copy is kept that acts as the
        representation of hosts when it was last written. This checks for any
        changes since then.

        Returns
        -------
        :class:`bool`
        """
        current = self.hosts
        last = self.written_hosts
        if len(list(current.keys())) != len(list(last.keys())):
            log.debug('keys different')
            return True
        for key in current.keys():
            hs_current = current.get(key)
            hs_last = last.get(key)
            if hs_last is None:
                log.debug('key missing')
                return True
            if hs_current != hs_last:
                log.debug(f'equality different')
                return True
        return False

    async def move_player(self,
                          player: typing.Optional[discord.Member],
                          voice_channel: discord.VoiceChannel,
                          event: typing.Dict[str, typing.Any]):
        """Moves a player safely into their team channel.

        Safely is defined as, the player has been registered as being in the
        game from the 'event' metadata that is given.

        Team channels have a maximum user threshold, and to prevent griefing
        from other members from jumping in and out of the voice channels, this
        method will move members in one at a time and remove anyone that does
        not belong before moving the next.

        Parameters
        ----------
        player: :class:`discord.Member`
            The Discord Member to move
        voice_channel: :class:`discord.VoiceChannel`
            The channel to move them to
        event: Dict[str, Any]
            The event data (request) from the REDIS server.

        Returns
        -------
        None
        """
        try:
            valid_players = [
                o.get('discord_id')
                for o in event.get('player_info', [])
                if o.get('discord_id') is not None
            ]
            for p in voice_channel.members:
                if p.id not in valid_players:
                    await p.move_to(
                        voice_channel.guild.afk_channel,
                        reason="Official Match Start. In Wrong Channel"
                    )
            if player is None or player.voice is None:
                return
            await voice_channel.set_permissions(player, read_messages=True)
            await player.move_to(voice_channel, reason="Official Match Start")
        except discord.errors.Forbidden:
            log.error("No Move_Member permissions on bot")
            return
        except discord.errors.HTTPException:
            log.error("Unknown error when moving member to voice channel")

    @tasks.loop(seconds=10)
    async def persisting_task(self):
        """Background task that runs to detect changes in the hosts container.

        This will run at the interval listed above and then do a check to see
        if the hosts container has been modified since the last time it was
        written. It will write to disk if that is the case and update the
        carbon copy.

        Returns
        -------
        None
        """
        if self.state_has_changed():
            with open(PATH, 'wb') as f:
                log.debug('writing recovery')
                dill.dump(self.hosts, f, -1)
                self.written_hosts = copy.deepcopy(self.hosts)
                #json.dump(self.hosts, f, default=lambda x: self.foo(x))

    @persisting_task.before_loop
    async def before_persisting_task(self):
        """Restore the last written record of hosts.

        The task is started when the cog is loaded, so this executes right at
        loading of the COG itself. It will check to see if any hosts have
        persisted to disk and restore them. It will then restart all the
        tasks for each HostSession.

        Returns
        -------
        None
        """
        log.debug('getting ready persisting')
        await self.bot.wait_until_ready()
        log.debug('ready')
        try:

            with open(PATH, 'rb') as f:
                log.debug('loading')
                self.hosts = dill.load(f)
                self.written_hosts = copy.deepcopy(self.hosts)
            log.debug('starting tasks')
            for hs in self.hosts.values():
                log.debug('recovering task')
                hs:HostSession
                log.debug(hs._checkins)
                await hs.start_tasks()
        except FileNotFoundError:
            log.error("Couldn't open recovery file for Queues")

    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    @commands.command(name='queue-fallback', aliases=('qf',))
    async def queue_add_fallback_channel(self, ctx: Context, ch: discord.VoiceChannel):
        guild_string = str(ctx.guild.id)
        if guild_string not in self.config.keys():
            self.config[guild_string] = {}
        self.config[guild_string]['fallback_channel'] = ch.id

        with open(COG_CONFIG, 'w') as f:
            json.dump(self.config, f)

    @commands.group('queue', aliases=('q',))
    async def queue_group(self, ctx: Context):
        """Manages Queues in our Match System

        This is the command group for all functionality. When invoked, it will
        always instanciate a new session for the player invoking.

        Parameters
        ----------
        ctx
            This is passed automatically.

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if not session:
            player = ctx.author.display_name
            log.info(f"Starting new Host Session for {player}: {self.hosts}")
            await ctx.invoke(self.queue_new_session, [])


    @queue_group.command(name='new-session', aliases=('new',))
    async def queue_new_session(self, ctx: Context, hosts: Greedy[discord.Member]) -> None:
        """Creates a new :class:`HostSession` for the user to manage state
        within the Queue system.

        |coro|

        .. note::
            When utilizing queue recovery with pickling, this can face GIL
            issues if the background tasks are not properly cancelled.

        Parameters
        ----------
        hosts: Greedy[:class:`discord.Member`]
            A list of members to add in as hosts.

        Returns
        -------
        None
        """
        originator = ctx.author.id
        log.debug('instanciating')
        session = HostSession(ctx.guild.id, originator, hosts)
        self.hosts[str(originator)] = session
        for id in hosts:
            self.hosts[str(id)] = session
        await session.create_channels()
        await session.originator.add_roles(session.host_role)
        for p in session.hosts:
            await p.add_roles(session.host_role)
        await session.start_tasks()
        log.debug('looping')
        log.debug('invoking')
        await ctx.invoke(self.queue_set_streamer, ctx.author)
        await ctx.send(f"New Host Session for {ctx.author}", delete_after=5)

    @queue_group.command(name='set-streamer', aliases=('ss',))
    async def queue_set_streamer(self, ctx: Context, streamer: discord.Member):
        """Opens a websocket with a particular streamer to zcleagues.com

        |coro|

        This socket connection is what allows the bot to talk with our server
        and make our queue system 'smart'. Using websockets, it detects when
        games have started, and is able to track which players to prioritize.

        .. note::
            This automatically gets called when invoking any of the helper
            commands to create a dashboard and sets it to the person that
            invokes the command. In most cases, a streamer will be the person
            that is also using the bot.

            If someone else is streaming using our provided overlays, you can
            set this to them and have all the event notifications pushed to
            their stream.

        .. note::
            It is also worth noting that you can call this command to 'refresh'
            a websocket connection.

        Parameters
        ----------
        streamer: :class:`discord.Member`
            The member you wish to set as the primary streamer.

        Returns
        -------
        None

        """

        session = self.get_host(ctx.author)
        if not session:
            return
        log.debug('open connection set streamer')
        await session.open_streamer_connection(streamer)
        log.debug('afer connection set streamer')
        await utils.reaction(session.socket.open, ctx)

    async def prepare_dashboard(
            self, dashboard_channel: discord.TextChannel,
            announcement_channel: discord.TextChannel = None):
        """

        Parameters
        ----------
        dashboard_channel
        announcement_channel

        Returns
        -------

        """
        pass

    @queue_group.command(name='prepare')
    async def queue_prepare(self, ctx: Context):
        """Prepares a new dashboard and match for a session.

        |coro|

        This is the base command that is called by any other helper commands
        that set league rules for particular matches.
        Parameters

        .. note::
            Calling this function multiple times will not cause it to lose any
            state. However, the reactions will get reset to the default. This
            means if a user is currently checked in, they will more than likely
            have to add, then remove, the reaction to successfully check out.

        Parameters
        ----------

        Returns
        -------
        None

        """
        log.debug('in prepare')
        session = self.get_host(ctx.author)
        if not session:
            log.debug('no session found in prepare')
            return
        if not session.is_originator(ctx.author):
            await ctx.send("Only the originator may use this command.", delete_after=5)
            return
        tb = constants.TWITCH_BOT
        await tb.join_channels([session.twitch_stream])
        twitch_ch: TwitchChannel = tb.get_channel(session.twitch_stream)
        await twitch_ch.send(textwrap.dedent(
            """
            Hi! We are monitoring Twitch Chat for this next game.
            Please keep conversation civil. Interested in joining?
            http://zcleagues.com/join
            """
        ))

        ch = discord.utils.get(ctx.guild.channels, name='general')
        await session.create_dashboard(ctx.channel, announcement_ch=ch)

    async def end_session(
            self, session: HostSession,
            fallback_channel: typing.Optional[discord.VoiceChannel]=None) -> None:
        """Ends the session for the user.

        |coro|

        This will remove the dict key from the cog self.hosts and destroy the
        HostSession instance. This is a cleanup function that stops the needed
        tasks and closes the websocket connection.

        .. note::
           All dashboards, announcements, or lobby roles will be deleted.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        originator = session.originator.display_name
        originator_id = session.originator.id
        ch = session.dashboard_channel
        await session.end(fallback_channel=fallback_channel)
        for host in session.hosts:
            try:
                del self.hosts[str(host.id)]
            except KeyError:
                log.warning(f"{host} not found in host keys")
                continue
        try:
            del self.hosts[str(originator_id)]
        except KeyError:
            log.warning(f"{originator_id} not found in host keys")

        if ch:
            await ch.send(
                f"Host session for {originator} has ended.",
                delete_after=5
            )


    @queue_group.command(name='end-session', aliases=('end',))
    async def queue_end_session(self, ctx: Context) -> None:
        """Ends the session for the user.

        |coro|

        This will remove the dict key from the cog self.hosts and destroy the
        HostSession instance. This is a cleanup function that stops the needed
        tasks and closes the websocket connection.

        .. note::
            All dashboards, announcements, or lobby roles will be deleted.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author.id)

        if not session:
            return
        if not session.is_originator(ctx.author):
            await ctx.send("Only the originator may use this command.", delete_after=5)
            return

        fallback = self.fallback_channel(ctx.guild)
        await self.end_session(session, fallback_channel=fallback)

    def find_session_by_attribute(self, attr: str, value: typing.Any) -> typing.Optional[HostSession]:
        for hs in self.hosts.values():
            try:
                attr_value = hs.__getattribute__(attr)
                result = attr_value == value
                log.debug(f"{type(attr_value)}:{type(value)}")
                log.debug(f"{attr_value} == {value}: {result}")
                if attr_value == value:
                    log.debug('passed equality. Returning')
                    return hs
            except AttributeError:
                log.debug(f"HostSession no attribute: {attr}")
                continue

    def get_session_by_dashboard(self, dashboard: typing.Union[int, discord.Message]) -> typing.Optional[HostSession]:
        """Iterates through the keys and finds a session by a dashboard
        message id.

        This function is used primarily on on_raw_reaction_* events that only
        send back the payload data that has a message id, and not enough info
        to get a session by the original host.

        Parameters
        ----------
        dashboard: Union[:class:`int`, :class:`discord.Message`]
            The message id, or message object of the dashboard.

        Returns
        -------
        Optional[:class:`HostSession`]
            the HostSession instance that the dashboard belongs to.

        """
        id = safe_value(dashboard)
        for hs in self.hosts.values():
            if id == hs._dashboard:
                return hs


    @queue_group.command(name='professional', aliases=('pro',))
    async def queue_for_pro(self, ctx: Context):
        """Prepares a dashboard for professional settings.

        |coro|

        This sets appropriate role restrictions on the server and other league
        specific settings. We save the command used to the session to make sure
        we can call upon it again seamlessly when a match ends.

        ..note::
            If the roles are not present on the server, the bot will attempt
            to make them. If that fails, no role restrictions are set.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if not session:
            return
        roles = [
            await utils.get_or_create_role(ctx.guild, 'Professional League'),
            await utils.get_or_create_role(ctx.guild, 'Temp Pro League'),
        ]
        roles = [r for r in roles if r is not None]

        session.winners_advance = True
        session.required_roles = roles
        session.command_invoked = self.queue_for_pro
        await ctx.invoke(self.queue_prepare)

    @queue_group.command(name='intermediate', aliases=('int',))
    async def queue_for_int(self, ctx: Context):
        """Prepares a dashboard for intermediate settings.

        |coro|

        This sets appropriate role restrictions on the server and other league
        specific settings. We save the command used to the session to make sure
        we can call upon it again seamlessly when a match ends.

        ..note::
            If the roles are not present on the server, the bot will attempt
            to make them. If that fails, no role restrictions are set.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if not session:
            return
        roles = [
            await utils.get_or_create_role(ctx.guild, 'Intermediate League'),
            await utils.get_or_create_role(ctx.guild, 'Temp Intermediate'),
        ]
        roles = [r for r in roles if r is not None]
        session.winners_advance = False
        session.required_roles = roles
        session.command_invoked = self.queue_for_int
        await ctx.invoke(self.queue_prepare)

    @queue_group.command(name='upandcoming', aliases=('uac', 'unc'))
    async def queue_for_uac(self, ctx: Context):
        """Prepares a dashboard for Up & Coming settings.

        |coro|

        This sets appropriate role restrictions on the server and other league
        specific settings. We save the command used to the session to make sure
        we can call upon it again seamlessly when a match ends.

        ..note::
            If the roles are not present on the server, the bot will attempt
            to make them. If that fails, no role restrictions are set.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if not session:
            return
        roles = [
            await utils.get_or_create_role(ctx.guild, 'Up and Coming League'),
        ]
        roles = [r for r in roles if r is not None]
        session.winners_advance = False
        session.required_roles = roles
        session.command_invoked = self.queue_for_uac
        await ctx.invoke(self.queue_prepare)

    @queue_group.command(name='unset-restrictions', aliases=('usr',))
    async def queue_usr(self, ctx: Context):
        """Removes all role restrictions from the session.

        |coro|

        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if not session:
            return
        session.required_roles = []
    @queue_group.command(name='pp')
    async def part_channel_test(self, ctx):
        session = self.get_host(ctx.author)
        if not session:
            return
        log.info("Parting channel")
        await constants.TWITCH_BOT.part_channels(['zcleagues'])

    @queue_group.command(name='set-twitch-stream', aliases=('sts',))
    async def match_set_twitch_stream(self, ctx: Context, value: str):
        channels = [hs.twitch_stream for hs in self.hosts.values()]
        channel_counts = collections.Counter(channels)
        session = self.get_host(ctx.author)
        if session:
            tb = constants.TWITCH_BOT
            current_channel = session.twitch_stream
            if current_channel.lower() == value.lower():
                return
            if channel_counts.get(current_channel) == 1:
                try:
                    await tb.part_channels([current_channel])
                except Exception as e:
                    # TODO: Make this non-broad. Couldn't find exact exception.
                    log.error(f"Could not part twitch channel {current_channel}: {e}")
            if channel_counts.get(value) is None:
                try:
                    await tb.join_channels([value])
                except Exception as e:
                    log.error(f"Could not join twitch channel {value}: {e}")
            session.twitch_stream = value
            twitch_ch: TwitchChannel = tb.get_channel(session.twitch_stream)
            await twitch_ch.send(textwrap.dedent(
                """
                Hi! We are monitoring Twitch Chat for this next game.
                Please keep conversation civil. Interested in joining?
                http://zcleagues.com/join
                """
            ))



    @queue_group.command(name='set-winners-advance', aliases=('swa',))
    async def match_set_winners_advance(self, ctx: Context) -> None:
        """Allow/Disallow of prioritization of winners.

        |coro|

        Winners advancement works by priortizing winners that check in to the
        front of the queue line. Using this command will toggle the feature
        on or off.

        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if session:
            session.winners_advance = not session.winners_advance
        await utils.reaction(session is not None, ctx)
        await utils.reaction(session.winners_advance, ctx)

    @queue_group.command(name='add-restrictions', aliases=('ar',))
    async def queue_add_restrictions(self, ctx: Context, roles: Greedy[discord.Role]):
        """Adds a role restriction to the session.

        |coro|

        A member must have this role to be allowed to 'check in' to play. These
        roles are just regular roles that you can assign to users freely in
        discord.

        Parameters
        ----------
        roles: List[:class:`discord.Role`]
            A list of roles to assign. These can be mentions or ids.

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if not session:
            return
        session.required_roles += roles

    @queue_group.command(name='remove-restrictions', aliases=('rr',))
    async def queue_remove_restrictions(self, ctx: Context, roles: Greedy[discord.Role]):
        """Adds a role restriction to the session.

        |coro|

        Parameters
        ----------
        roles: List[:class:`discord.Role`]
            A list of roles to remove. These can be mentions or ids.

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if not session:
            return
        session.required_roles = [r for r in session.required_roles if r not in roles]

    @queue_group.command(name='leave')
    async def queue_leave(self, ctx: Context):
        """
        Helper command to leave as a host if needed.

        Parameters
        ----------
        ctx

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if not session:
            return
        if session.is_originator(ctx.author):
            await self.end_session(session)
        else:
            session.hosts = [h for h in session.hosts if h.id != ctx.author.id]
            del self.hosts[str(ctx.author.id)]



    @queue_group.command(name='add-hosts', aliases=('ah',))
    async def queue_add_hosts(self, ctx: Context, hosts: Greedy[discord.Member]) -> None:
        """Adds hosts to the session to help with administration.

        |coro|

        A host is someone who can directly manipulate a dashboard and the
        session settings with commands. This will add a reference of HostSession
        to the list of hosts supplied.

        Parameters
        ----------
        ctx
        hosts: List[:class:`discord.Member`]
            The members you want to make hosts

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if session:
            for dm in hosts:
                player = dm.display_name
                if str(dm.id) in self.hosts.keys():
                    # Already a host somewhere.
                    await ctx.send(
                        f"Ignoring {player}. Another session exists.",
                        delete_after=5
                    )
                self.hosts[str(dm.id)] = session
                session.hosts = session.hosts + [dm]
                if session.host_role:
                    await dm.add_roles(session.host_role)

    @queue_group.command(name='del-hosts', aliases=('dh',))
    async def queue_del_hosts(self, ctx: Context, hosts: Greedy[discord.Member]) -> None:
        """Deletes hosts to the session to help with administration.

        |coro|

        A host is someone who can directly manipulate a dashboard and the
        session settings with commands. This will add a reference of HostSession
        to the list of hosts supplied.

        Parameters
        ----------
        ctx
        hosts: List[:class:`discord.Member`]
            The members you want to remove as hosts
        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if not session:
            return
        to_remove = [
            h
            for h in hosts
            if h in session.hosts and not session.is_originator(h)
        ]
        session.hosts = [h for h in session.hosts if h not in to_remove]
        for dm in to_remove:
            try:
                player = dm.display_name
                del self.hosts[str(dm.id)]
            except KeyError:
                log.warning(f"Session was already non-existent for {player}")
                continue

    @queue_group.command(name='link', aliases=('l',))
    async def queue_link_profile(self, ctx: Context, name: str, player: discord.Member):
        """Links a handle to a profile on the backend database.

        Starcraft uses Handles and we track membership through Discord UserIds.
        When a game is rolled in, our database will attempt to match that handle
        to a discord user. This is done so we can track state management for
        player prioritization. However, if we cannot find that discord user, an
        unlinked handle exists. This command will associate it with the
        discord user.

        Parameters
        ----------
        ctx:
            Passed automatically.
        name: str
            The handle name to link. CASE SENSITIVE
        player: :class:`discord.Member`
            The Discord Member to link to.

        Returns
        -------
        None
        """
        session = self.get_host(ctx.author)
        if not session:
            return
        channel = ctx.channel
        try:
            matched_handles = [o for o in session.unlinked_profiles if name == o.get('name')]
            handle_string = None if len(matched_handles) == 0 else matched_handles[0].get('handle_string')
            if not handle_string:
                await channel.send("Error, handle could not be found on player.", delete_after=10)
                return
            handle = await restapi.get('handle', handle_string)
            site_player = await restapi.get('discordplayer', player.id)
            if site_player:
                handle['player'] = site_player
                await restapi.save('handle', handle, id_key='handle')
                await channel.send('Handle Linked!', delete_after=10)

                # Remove from unlinked
                session.unlinked_profiles = [p for p in session.unlinked_profiles if
                                             p.get('handle_string') != handle_string]

                for obj in matched_handles:
                    obj['discord_id'] = player.id
                # Just replace with the player object. This will track how many times they've been rolled in.
                matched_handles = [player for _ in matched_handles]
                session.rolled_in += matched_handles
                # Update any winner keys
                for m in session.matches:
                    for w in session.matches[m].get('winners', []):
                        if w['handle'] == handle_string:
                            w['discord_id'] = player.id
                unlinked_msg = await session.unlinked_embed
                if unlinked_msg:
                    if len(session.unlinked_profiles) == 0:
                        await utils.delete_message(unlinked_msg)
                    else:
                        m = await unlinked_msg.edit(embed=session.generate_unlinked_embed())
                        session.unlinked_embed = m
            else:
                await channel.send("Error, could not locate an account for that discord player.")

        except IndexError:
            await channel.send('Invalid ID to link')



    @commands.Cog.listener()
    async def on_raw_reaction_add(
            self, raw: discord.RawReactionActionEvent) -> None:
        """Event listener for processing checkins

        |coro|

        This event listener captures any reaction add to a message & attempts to
        pull a session from the message id. It assumes that this is a dashboard
        if a session is found.

        ..note::
            We use raw_reaction_add to get the contents of messages after the
            bot reconnects in case connectivity is lost.

        Parameters
        ----------
        raw: class:`discord.RawReactionActionEvent`
            The payload from discord reaction

        Returns
        -------
        None
        """
        source_session = self.get_session_by_dashboard(raw.message_id)
        if source_session is None:
            return
        user = discord.utils.get(source_session.guild.members, id=raw.user_id)
        if not user or user == self.bot.user:
            return
        for num, host_session in enumerate(self.hosts.values()):
            if host_session == source_session:
                if not host_session.is_checked_in(user):
                    await host_session.process_checkin(raw.emoji, user, checkin=True)
            else:
                if host_session.is_checked_in(user):
                    # checked in on another dashboard. Remove from other.
                    await host_session.remove_player_reaction(user)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, raw: discord.RawReactionActionEvent) -> None:
        """Event listener for processing checkouts

        |coro|

        This event listener captures any reaction remove & attempts to
        pull a session from the message id. It assumes that this is a dashboard
        if a session is found.

        ..note::
            We use raw_reaction_remove to get the contents of messages after the
            bot reconnects in case connectivity is lost.

        Parameters
        ----------
        raw: class:`discord.RawReactionActionEvent`
            The payload from discord reaction

        Returns
        -------
        None
        """
        session = self.get_session_by_dashboard(raw.message_id)
        if not session:
            return
        user = discord.utils.get(session.guild.members, id=raw.user_id)
        if not user or user == self.bot.user:
            return
        if session:
            await session.process_checkin(raw.emoji, user, checkin=False)

    @commands.Cog.listener()
    async def on_session_timeout(self, session: HostSession):
        """Event listener for when a session times out

        |coro|

        A session has a built in timer for when last activity was detected on
        a host. Activity is defined as when the dashboard is visible.

        If the activity crosses the threshold, this event is triggered.

        Parameters
        ----------
        session: :class:`HostSession`
            The HostSession instance that has timed out.

        Returns
        -------
        None
        """
        host = session.originator.display_name
        log.info(f"{host} session timeout. Destroying session.")
        await self.end_session(session)

    @commands.Cog.listener()
    async def on_command_completion(self, ctx: Context):
        """Event to capture successful commands from this cog.

        This main purpose is to just facilitate a quicker update to the
        dashboard and then clean up the command that way the dashboard does
        not get lost in a series of commands typed in a channel.

        Parameters
        ----------
        ctx
            The Discord Context of the command issued.
        Returns
        -------
        None
        """
        if ctx.cog != self:
            return
        session = self.hosts.get(ctx.author)
        if session:
            db = await session.dashboard
            if db:
                m = await db.edit(embed=session.generate_dashboard())
                session.dashboard = m
        await asyncio.sleep(5)
        await ctx.message.delete()



    @commands.Cog.listener()
    async def on_match_checkin(self, user, session):
        """Event for when a player checks into a dashboard.

        This communicates to our REDIS server a new event for checkin. This
        communication allows our Twitch Overlays to update with information when
        a player checks in to display on the stream.

        Parameters
        ----------
        user: :class:`discord.Member`
            The Discord Member that checked in
        session: :class:`HostSession`
            The HostSession (dashboard) that they checked into.

        Returns
        -------
        None
        """
        if session.socket is not None and session.socket.open:
            session.socket: websockets.WebSocketClientProtocol
            checkin = json.dumps({
                'type': 'match_checkin',
                'player_avatar': str(user.avatar_url),
                'player_id': user.id,
                'player': user.display_name,
            })
            await session.socket.send(data=checkin)

    @commands.Cog.listener()
    async def on_match_checkout(self, user, session):
        """Event for when a player checks out of a dashboard.

        This communicates to our REDIS server a new event for checkout. This
        communication allows our Twitch Overlays to update with information when
        a player checks out and removes it from the stream.

        Parameters
        ----------
        user: :class:`discord.Member`
            The Discord Member that checked in
        session: :class:`HostSession`
            The HostSession (dashboard) that they checked into.

        Returns
        -------
        None
        """
        if session.socket is not None and session.socket.open:
            session.socket: websockets.WebSocketClientProtocol
            checkin = json.dumps({
                'type': 'match_checkout',
                'player_avatar': str(user.avatar_url),
                'player_id': user.id,
                'player': user.display_name,
            })
            await session.socket.send(data=checkin)

    @commands.Cog.listener()
    async def on_ws_event_request_checkins(self, session, event):
        """Event for when a request is received for the entire checkin list.

        This is a helper event that is special coded to refresh the current
        checkin state. This is to mitigate if a streamer switches scenes.
        Since the overlays are browser sources, this would cause a page refresh
        to occur and state lost.

        Therefore, when a browser source refreshes, this is the first request
        that is sent to re-update everyone that has already checked in.

        Parameters
        ----------
        session: :class:`HostSession`
            The HostSession for which the page source is requesting.
        event: Dict[str, Any]
            The event data (request) that was received from the server.

        Returns
        -------
        None
        """
        if session.socket is not None and session.socket.open:
            container = []
            for p in session.checkins:
                p: discord.Member
                obj = {
                    'player': p.display_name,
                    'player_avatar': str(p.avatar_url),
                    'player_id': p.id,
                }
                container.append(obj)
            response = {
                'type': 'response_checkins',
                'checkins': container
            }
            await session.socket.send(data=json.dumps(response))






    @commands.Cog.listener()
    async def on_ws_event_match_start(self, session, event):
        """Event for when a match_start type is received from REDIS.

        State management for when a match_start signal is intercepted by
        the bot. There are a couple of items we need to track for the queue
        manager.
        - How many times has a player played?
        - How many times has a player checked in, but got rolled out,
        - Are there any handles that don't have a discord link to them?

        Rolls are tracked by session.rolled_in and session.rolled_out. They will
        contain duplicate entries of a discord.Member object for every time a
        member either rolls in or out.

        Unlinked profiles are captured from the bot by the 'player_info' key
        which will have a None object for the 'discord_id' field. These will get
        appended to unlinked_roles in duplicate fashion much like roles in. Once
        linked it will be merged with the rolled_in list object.

        This will also automatically move players into their team lobbies
        as long as they have a linked handle in the database.

        Parameters
        ----------
        session: :class:`HostSession`
            The HostSession for which the page source is requesting.
        event: Dict[str, Any]
            The event data (request) that was received from the server.

        Returns
        -------
        None

        """
        guild = session.guild
        db = await session.dashboard
        log.debug(f"Match Start: Lobby Channel: {session.lobby_channel}")
        unlinked_embed = await session.unlinked_embed
        rolled_in = [discord.utils.get(session.guild.members, id=o.get('discord_id'))
                     for o in event.get('player_info')
                     if o is not None]
        # Rolls in may be None is a non-discord member is randomized into the match. Don't track.
        rolled_in = [ri for ri in rolled_in if ri is not None]
        session.rolled_in += rolled_in
        session.unlinked_profiles += [
            o for o in event.get('player_info', [])
            if o.get('discord_id') is None
        ]
        session.rolled_out += [p for p in session.checkins if p not in rolled_in]
        match_id = event.get('match_id')
        session.matches[match_id] = {}

        # Make announcement
        embed = utils.BrandedEmbed(title=f"Match {event.get('match_id')} Started!")
        embed.add_field(name="Streamer", value=session.streamer.display_name)
        embed.add_field(name="Game #", value=str(len(session.matches)))
        players = ', '.join(p.get('name') for p in event.get('player_info'))
        embed.add_field(name='Players', value=players)
        await session.dashboard_channel.send(embed=embed)
        session.dashboard = await utils.delete_message(db)
        if session.unlinked_profiles:
            session.unlinked_embed = await utils.delete_message(unlinked_embed)
            session.unlinked_embed = await session.dashboard_channel.send(
                embed=session.generate_unlinked_embed()
            )
        observers = event.get('observer_info', [])
        observers = [discord.utils.get(session.guild.members, id=id) for id in observers]
        observers = [o for o in observers if o is not None]
        if session.streamer_channel:
            for p in session.hosts:
                await utils.move_to_voice(
                    p, session.streamer_channel, reason="Match Start Streamer"
                )
            await utils.move_to_voice(
                session.originator, session.streamer_channel, reason="Match Start Streamer"
            )
            for o in observers:
                await utils.move_to_voice(
                    o, session.streamer_channel, reason="Match Start Streamer"
                )


        vc_names = [
            "Team Ramrod",
            "Average Joe's",
            "Orange or Bust",
            "OMG WTF BBQ",
            "Quack!",
            "Terrible Play!",
            "Zony McZoneface",
            "Nerf Reapers",
            "Dat RNG Baby!",
            "Rauds OP",
            "Squeaker Central",
            "Scout? What's that?",
            "Switching is Life",
            "Hold the Nuke"
        ]
        random.shuffle(vc_names)
        num_teams = max([
                int(p.get('team_number', 0))
                for p in event.get('player_info', [])
        ]) + 1
        overwrites = {
            guild.default_role: discord.PermissionOverwrite(read_messages=False),
            session.host_role: discord.PermissionOverwrite(read_messages=True)
        }
        team_channels = []
        for ch_name in vc_names[:num_teams]:
            team_channels.append(
                await session.guild.create_voice_channel(
                    ch_name,
                    overwrites=overwrites,
                    category=session.category
                )
            )
        session.team_channels = team_channels
        for p in event.get('player_info', []):
            if not p.get('discord_id') or p.get('team_number') is None:
                continue
            team_number = int(p.get('team_number'))
            voice_channel = session.team_channels[team_number]
            member = discord.utils.get(session.guild.members, id=p.get('discord_id'))
            if voice_channel is not None:
                await self.move_player(member, voice_channel, event)
        announcement = await session.announcement
        if announcement:
            session.announcement = await utils.delete_message(announcement)
        log.debug(f"Match Start: Lobby Channel: {session.lobby_channel}")

    async def simple_context(self, session: HostSession) -> typing.Optional[Context]:
        if session.dashboard_channel:
            last_msg_id = session.dashboard_channel.last_message_id
            last_msg = await session.dashboard_channel.fetch_message(last_msg_id)
            ctx = await self.bot.get_context(last_msg)
            ctx.author = session.originator
            return ctx

    @commands.Cog.listener()
    async def on_twitch_message(self, message: twitchio.Message):
        #ch = discord.utils.get(self.bot.guilds[0].channels, name='contributor-lounge')
        #await ch.send(f"From Twitch {message.channel}: {message.content}")
        session = self.find_session_by_attribute('twitch_stream', message.channel.name)
        if session:
            log.debug("Found hostsession")
            payload = {
                'type': 'twitch',
                'subtype': 'message',
                'payload': {
                    'author': message.author.name,
                    'channel': message.channel.name,
                    'tags': message.tags,
                    'content': message.content,
                    'timestamp': message.timestamp.isoformat()
                }
            }
            await session.send_data(json.dumps(payload))
        else:
            log.debug("COULD NOT FIND HOSTSESSION")

    @commands.Cog.listener()
    async def on_ws_event_match_end(self, session, event):
        """Event for when a match_end type is detected from REDIS

        When a player is running our ZC Listener, an event match_end is sent
        whenever the game ends or the player types the specific -match_end
        command in all-chat as an observer.

        This aims to intercept the event and update the dashboards and winenrs
        to allow player prioritization.

        Parameters
        ----------
        session: :class:`HostSession`
            The HostSession for which the match ended
        event: Dict[str, Any]
            The event data (Request) that was received from the server)

        Returns
        -------
        None
        """
        log.debug("In match end")
        log.debug(f"Match End: Lobby Channel: {session.lobby_channel}")
        session.last_activity = utils.utc_now()
        session.checkins = []
        try:
            tts:GoogleTTSCOG = self.bot.get_cog('GoogleTTS')
            session.matches[event.get('match_id')]['winners'] = event.get('winners')
            winners = ' & '.join(o.get('name') for o in event.get('winners')) or "No Players (DRAW)"
            winning_players = [o.get('discord_id') for o in event.get('winners', [])]
            winning_players = [id for id in winning_players if id is not None]
            winning_players = [
                discord.utils.get(session.guild.members, id=id)
                for id in winning_players
            ]
            ch = None
            for p in winning_players:
                p:discord.Member
                if p is None or p is not None and not p.voice:
                    continue
                if p.voice.channel != ch:
                    if tts:
                        await tts.speak(p.voice.channel, "Prepare to be moved for interview")
                        await asyncio.sleep(5)
                ch = p.voice.channel
                p:discord.Member
                await utils.move_to_voice(p, session.streamer_channel, reason="Interview")

            non_winners = [p for p in session.checkins if p not in winning_players]
            for p in non_winners:
                await utils.move_to_voice(p, session.lobby_channel)

            await session.delete_stateful(session.team_channels)
            session.team_channels = []

            # for vc in session.team_channels:
            #    await vc.delete(reason="Match over")

            desc = f'Winners set to {winners}'
            embed = utils.BrandedEmbed(title='Match Ended!', description=desc)
            if session.announcement_role:
                for p in session.checkins:
                    await p.remove_roles(session.announcement_role)

            channel = session.dashboard_channel
            if not channel:
                log.debug("No dashboard channel")
                return
            await channel.send(embed=embed)
            cmd_to_invoke = self.queue_prepare if session.command_invoked is None else session.command_invoked


            # TODO: Kill this with fire. Is there a better way to do this?
            log.debug("Getting simple context to invoke dashboard")
            ctx = await self.simple_context(session)
            if ctx:
                await ctx.invoke(cmd_to_invoke)

        except KeyError:
            log.error(f"Could not locate match {event.get('match_id')} to update winners")
            embed = utils.BrandedEmbed(title='Match Ended!', description='No Winners Updated')



def setup(bot):
    bot.add_cog(Queues(bot))
    log.info("Cog loaded: Queues")