from discord.ext.commands import Context, Bot, command, Cog
from bot import webapi
from discord import Embed
from bot.pagination import LinePaginator
from bot import constants
import discord
import logging
from aiogoogletrans import Translator
from aiogoogletrans.models import Translated
from aiogoogletrans.constants import LANGUAGES
from bot import utils
import datetime
from datetime import timedelta
from bot.utils import uniques

log = logging.getLogger(__name__)

RECENT = {}
class Translate(webapi.WebAPIMixin, Cog):

    def __init__(self, bot: Bot):
        self.bot = bot
        self.translate_api = "https://translate.google.com/m?h1={to_language}&sl=auto&q={msg}"
        self.translator = Translator()

    async def detect_language(self, message: str):
        tr = Translator()
        result = await tr.detect(message)
        return result.lang

    @command(name='translate', aliases=('tr',))
    async def translate_translate(self, ctx: Context, to_language: str, *msg: str):
        msg = ' '.join(msg)
        tr = Translator()
        translation = await tr.translate(msg, dest=to_language)
        await ctx.send(translation.text)

    @command(name='translate-languages', aliases=('trlangs',))
    async def translate_languages(self, ctx: Context):
        embed = utils.BrandedEmbed(title="Available Languages")

        await LinePaginator.paginate(
            lines=(f"{v}: '{k}'" for k,v in LANGUAGES.items()),
            ctx=ctx,
            embed=embed,
            empty=False,
            max_lines=constants.PAGINATION_MAX_LINES,
        )

    @Cog.listener()
    async def on_message(self, message: discord.Message):
        if isinstance(message.channel, discord.DMChannel):
            return
        if message.channel.name == 'global' and message.author != self.bot.user:
            tr = Translator()
            en:Translated = await tr.translate(message.clean_content, dest='en')
            source = en.src
            self.log_recent(source)
            description = message.clean_content
            embed = discord.Embed(description=description)
            embed.set_author(
                name=f"{message.author.display_name} ({self.language(source)})",
                icon_url=message.author.avatar_url
            )
            #embed.add_field(name=f"Source ({LANGUAGES.get(source)})", value=message.clean_content or "\u200b", inline=False)
            if source != 'en':
                embed.add_field(name="english", value=en.text or "\u200b", inline=False)
            for lang in uniques(['es'] + list(RECENT.keys())):
                t = await tr.translate(message.clean_content, dest=lang)
                if lang != t.src:
                    name = self.language(t.dest)
                    embed.add_field(name=name, value=t.text or "\u200b", inline=False)
            await message.channel.send(embed=embed)
            await message.delete()
            return

        #await self.bot.process_commands(message)

    def log_recent(self, src):
        now = datetime.datetime.now()
        if src != 'en':
            RECENT[src] = now
        for key, value in list(RECENT.items()):
            if key == src:
                continue
            if timedelta(minutes=2) < now - value:
                del RECENT[key]


    def language(self, src):
        lang = LANGUAGES.get(src)
        if lang:
            return lang.capitalize()
        return "Unknown"

def setup(bot: Bot):
    bot.add_cog(Translate(bot))
    log.info("Cog Loaded: Translate")