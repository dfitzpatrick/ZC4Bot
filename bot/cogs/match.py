import logging

import discord
from discord.ext import commands

from bot import constants

import typing
import json
import dateutil
from bot.mixins.config import ConfigMixin
log = logging.getLogger(__name__)

ANNOUNCEMENT_TIMEOUT = 60*60 # 1 hour

class Match(ConfigMixin, commands.Cog):
    """
    COG Responsible for handling anything ZCl Match related with discord
    """

    def __init__(self, bot: commands.Bot):
        super(Match, self).__init__()
        self.bot = bot
        self.messenger = None


    @commands.Cog.listener()
    async def on_ready(self):
        log.debug('in ready for match cog')
        self.messenger = self.bot.get_cog('Messenger')
        log.debug(self.messenger)

        for guild in self.bot.guilds:
            id = str(guild.id)
            cfg_matches = self.config_settings[id].get('matches')
            if cfg_matches is None:
                self.config_settings[id]['matches'] = {}
        self.save_settings()

    def users_to_teams(self, rosters: constants.PAYLOAD) -> typing.Dict[int, typing.List[str]]:
        """
        Helper function to help organize discord users into teams
        Parameters
        ----------
        rosters

        Returns
        -------
        typing.Dict[int, typing.List[str]]
        """
        teams = {}
        for r in rosters:
            team_number = r['team_number']
            if team_number not in teams.keys():
                teams[team_number] = []
            teams[team_number] += r['discord_users']
        return teams

    async def move_member(self, member: discord.Member, channel: discord.VoiceChannel):
        try:
            if member.voice is not None:
                await member.move_to(channel, reason="Game Start")
        except discord.errors.Forbidden:
            log.debug('Requiring Move Member Permissions')

    def rosters(self, rosters: constants.PAYLOAD, guild: discord.Guild):
        user_teams = self.users_to_teams(rosters)
        for team_number, users in user_teams.items():
            users = [discord.utils.get(guild.members, id=int(u)) for u in users]
            users = [u for u in users if u is not None]
            if len(users) == 0:
                continue

    async def new_match_embed(self, payload: constants.PAYLOAD):
        has_streamers = len(payload['streamers']) > 0
        title = "New Match LIVE STREAMED" if has_streamers else "New Client Connected Match"
        embed = discord.Embed(title=title, color=discord.Color.green())
        match = payload['match']
        streamers = payload['streamers']
        id = match['id']
        created = dateutil.parser.parse(match['match_date'])
        status = match['status']
        players = ', '.join([p['sc2_profile']['name'] for p in match['rosters']]) or 'None'
        stream_players = '\n'.join(f"[{p['profile']['name']}](https://twitch.tv/{p['stream']['username']})" for p in streamers)
        if status != 'initial':
            return
        embed.add_field(name='ID', value=id)
        embed.add_field(name='Players', value=players, inline=False)
        if stream_players != '':
            embed.add_field(name='Streams', value=stream_players, inline=False)
        embed.timestamp = created
        return embed



    @commands.Cog.listener()
    async def on_match_delete(self, guild: int, payload: constants.PAYLOAD):
        log.debug(payload)
        game_id = str(payload['id'])
        cfg_game = self.config_settings[str(guild)].get('matches', {}).get(game_id)
        log.debug(self.config_settings[str(guild)])
        log.debug(cfg_game)
        if cfg_game is None:
            return
        for o in cfg_game.get('announcement_messages', []):
            ch_id = o['channel']
            msg_id = o['msg']
            channel:discord.TextChannel = self.bot.get_channel(ch_id)
            if channel is None:
                continue
            message = await channel.fetch_message(msg_id)
            if message is None:
                continue
            try:
                await message.delete()
            except (discord.errors.NotFound, discord.errors.Forbidden):
                pass
        cfg_game['announcement_messages'] = []
        self.config_settings[str(guild)][game_id] = cfg_game
        self.save_settings()



    @commands.Cog.listener()
    async def on_new_match(self, guild: int, payload: constants.PAYLOAD):
        log.debug(payload)
        game_id = str(payload['match']['id'])
        guild = self.bot.get_guild(guild)
        has_streamers = len(payload['streamers']) > 0
        log.debug(self.messenger)
        embed = await self.new_match_embed(payload)
        if self.messenger is not None:
            if has_streamers:
                thumb_url = payload['streamers'][0]['stream']['extra_data']['data'][0]['thumbnail_url'].format(
                    width=512, height=512)
                embed.set_thumbnail(url=thumb_url)
                msgs = await self.messenger.announcement_messenger(guild, embed=embed, delete_after=ANNOUNCEMENT_TIMEOUT)

                cfg_matches = self.config_settings[str(guild.id)]['matches']
                cfg_game = cfg_matches.get(game_id, {})
                cfg_game['announcement_messages'] = [{k:v.id for k,v in m.items()} for m in msgs]
                cfg_matches[game_id] = cfg_game
                self.config_settings[str(guild.id)]['matches'] = cfg_matches
                self.save_settings()
            await self.messenger.logging_messenger(guild, embed=embed)


    @commands.has_permissions(administrator=True)
    @commands.command(name='mtest')
    async def testmatch_cmd(self, ctx: commands.Context):
        data = """{
   "match":{
      "id":"1596987331",
      "rosters":[
         {
            "id":89141,
            "sc2_profile":{
               "id":"1-S2-1-951651",
               "created":"2020-04-30T20:14:23.130640Z",
               "name":"Draconis",
               "profile_url":"https://starcraft2.com/en-us/profile/1/1/951651",
               "avatar_url":"https://static.starcraft2.com/starport/d0e7c831-18ab-4cd6-adc7-9d4a28f49ec7/portraits/11-24.jpg",
               "discord_users":[
                  "224734305581137921"
               ]
            },
            "lane": null,
            "created":"2020-08-09T22:35:37.725359Z",
            "updated":"2020-08-09T22:35:37.725380Z",
            "team_number":0,
            "position_number":0,
            "color":"84,0,129",
            "elo":0,
            "leaderboard_ranking":0,
            "match":"1596987331",
            "team": null
         },
         {
            "id":89142,
            "sc2_profile":{
               "id":"",
               "created":"2020-04-30T20:16:18.012226Z",
               "name":"FOO",
               "profile_url":"",
               "avatar_url":"None",
               "discord_users":[

               ]
            },
            "lane": null,
            "created":"2020-08-09T22:35:37.754861Z",
            "updated":"2020-08-09T22:35:37.754881Z",
            "team_number":0,
            "position_number":1,
            "color":"0,66,255",
            "elo":0,
            "leaderboard_ranking":0,
            "match":"1596987331",
            "team":"None"
         }
      ],
      "observers":[

      ],
      "created":"2020-08-09T22:35:37.688097Z",
      "updated":"2020-08-09T22:35:37.702019Z",
      "match_date":"2020-08-09T22:35:37.687805Z",
      "game_length":0.0,
      "game_id":"",
      "stream_url": null,
      "status":"initial",
      "draw":false,
      "ranked":false,
      "legacy":false,
      "guild": null,
      "arcade_map": null,
      "league": null,
      "season": null,
      "teams":[

      ]
   },
   "streamers":[
      {
         "profile":{
            "id":"1-S2-1-951651",
            "created":"2020-04-30T20:14:23.130640Z",
            "name":"Draconis",
            "profile_url":"https://starcraft2.com/en-us/profile/1/1/951651",
            "avatar_url":"https://static.starcraft2.com/starport/d0e7c831-18ab-4cd6-adc7-9d4a28f49ec7/portraits/11-24.jpg",
            "discord_users":[
               "224734305581137921"
            ]
         },
         "stream":{
            "id":12,
            "user":{
               "id":"224734305581137921",
               "avatar_url":"https://cdn.discordapp.com/avatars/224734305581137921/bcc04c2266573f4ee7fafc2c812063fb.png",
               "created":"2020-04-30T19:32:53.692669Z",
               "last_login":"2020-08-01T06:09:13.602532Z",
               "is_superuser":true,
               "first_name":"",
               "last_name":"",
               "is_staff":true,
               "is_active":true,
               "date_joined":"2020-04-30T19:32:53.692157Z",
               "updated":"2020-08-09T22:35:37.289834Z",
               "username":"Draconis",
               "email":"dfitz.murrieta@gmail.com",
               "discriminator":6776,
               "avatar":"bcc04c2266573f4ee7fafc2c812063fb",
               "client_heartbeat":"2020-08-09T22:35:37.262000Z",
               "groups":[

               ],
               "user_permissions":[

               ]
            },
            "uuid":"7053297b-c6b3-4a22-b4fe-84e6125d82f9",
            "username":"draconis184",
            "active":true,
            "extra_data":{
               "data":[
                  {
                     "id":"3985260125748817686",
                     "type":"live",
                     "title":"Random Acts of Code",
                     "game_id":"",
                     "tag_ids":[
                        "6ea6bca4-4712-4ab9-a906-e3336a9d8039"
                     ],
                     "user_id":"216834360",
                     "language":"en",
                     "user_name":"draconis184",
                     "started_at":"2020-08-09T22:31:35Z",
                     "viewer_count":0,
                     "thumbnail_url":"https://static-cdn.jtvnw.net/previews-ttv/live_user_draconis184-{width}x{height}.jpg"
                  }
               ]
            }
         }
      }
   ]
}"""

        payload = json.loads(data)
        has_streamers = len(payload['streamers']) > 0
        #await self.log_new_match(data)
        log.debug(self.messenger)
        embed = await self.new_match_embed(payload)
        if self.messenger is not None:
            if has_streamers:
                thumb_url = payload['streamers'][0]['stream']['extra_data']['data'][0]['thumbnail_url'].format(width=512, height=512)
                embed.set_thumbnail(url=thumb_url)
                await self.messenger.announcement_messenger(ctx.guild, embed=embed)

            await self.messenger.logging_messenger(ctx.guild, embed=embed)






def setup(bot: commands.Bot):
    bot.add_cog(Match(bot))
    log.info("Cog loaded: ZCL Match")
