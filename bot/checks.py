from discord.ext.commands import Context
from discord.ext.commands import check
from discord.utils import get
from discord.ext import commands

class RoleDoesNotExist(commands.CheckFailure):
    pass

def role_exists(role_name: str):
    async def predicate(ctx: Context):
        if get(ctx.guild.roles, name=role_name) is None:
            raise RoleDoesNotExist(role_name)
        return True
    return check(predicate)
